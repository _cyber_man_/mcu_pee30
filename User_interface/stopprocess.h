/*
 * stopprocess.h
 *
 *  Created on: 10 dec. 2019
 *      Author: 0904269
 */

#ifndef STOPPROCESS_H_
#define STOPPROCESS_H_

/* Includes */
#include <stdint.h>
#include <stddef.h>
#include <ti/drivers/GPIO.h>
#include "Board.h"

/* Defines */
#define MCU_GPIO_STOP_BUTTON    Board_GPIO_BUTTON0

/* Structures */
typedef enum
{
    MCU_idle,
    MCU_in_operation,
    MCU_safety_mode,
    MCU_terminate
} enum_MCU_operational_states;
enum_MCU_operational_states MCU_operational_states = MCU_idle;

#endif /* STOPPROCESS_H_ */
