/* C header files */
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>
/* RTOS header files */
#include <ti/sysbios/BIOS.h>
/* POSIX Header files */
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>
#include <mqueue.h>
#include <pthread.h>
#include "Board.h"
#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/PWM.h>
#include <ti/drivers/uart/UARTCC32XX.h>
#include <ti/drivers/gpio/GPIOCC32XX.h>
#include <ti/drivers/pwm/PWMTimerCC32XX.h>


/* Eigen headers */
#include "uart_term.h"
#include "bestanden.h"
#include <ti/drivers/GPIO.h>
//#include "external_globals.h"
//#include "startprocess.c"
//#include "startprocess.h"
//#include "stopprocess.h"


InitStartProcess();

// Taken
void *App_Task(void *args);

// Functies
void PingResultaat(SlNetAppPingReport_t *pReport);
int StelWifiIn(SlWlanSecParams_t *SecParams, SlWlanSecParamsExt_t *SecExtParams, _i8 *wachtwoord, bool vraagOmWachtwoord);
void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args));

// Semaphore voor taaksynchronisatie
sem_t wachtenOpIP;

// Wifi instellingen
#define IDENTITY ""
//#define SSID "AndroidLP"
//#define netwerkww "tiesiepie ajpie"
#define SSID "taart"
#define netwerkww "abcdefgh"
//#define SSID "AndroidDV"
//#define netwerkww "Enkjelftw"
//#define SSID "eduroam"
//#define netwerkww ""

//PWM_Handle PWMhandle;
int draaiknop = 0;
int phase = 0;
_i16 debounce = 0;
int debounce_safe = 0;
bool draaiknopstate = true;
bool startInitButton = false;
bool StartButton = false;
bool Gereed = false;
bool init = false;
bool Stopknop = false;

#define PWM_Pin     Board_PWM0

InitStartProcess();
int parameter_arr[35];
int manueel_arr[8];

void gpioButtonRotary(uint_least8_t index) //pin 6 (en 7)
{
    UART_PRINT("test interrupt 15.\r\n");
    GPIO_disableInt(Board_GPIO_ROTARY_A);

    if (GPIO_read(Board_GPIO_ROTARY_B)==0){
        if(draaiknop != 0){
            draaiknop--;
            UART_PRINT("%d RPM\r\n", draaiknop);
        }
    }
    else if (GPIO_read(Board_GPIO_ROTARY_B)==1){
        draaiknop++;
        UART_PRINT("%d RPM\r\n", draaiknop);
    }
    __delay_cycles(100);
    draaiknopstate = true;
    GPIO_enableInt(Board_GPIO_ROTARY_A);
}

void gpioButtonStop(uint_least8_t index) //pin 18
{
    /*Stop*/
//    GPIO_disableInt(Board_GPIO_STOP);
    UART_PRINT("test interrupt stop.\r\n");
    GPIO_toggle(Board_GPIO_LED17);
    Stopknop = true;
}

void gpioButtonStart(uint_least8_t index) //pin 62
{
    /*Start init & Start*/
//    GPIO_disableInt(Board_GPIO_START);
    UART_PRINT("test interrupt start.\r\n");
    StartButton = true;
}

void gpioButtonGereed(uint_least8_t index)//pin 8
{
    /*Gereed*/
//    GPIO_disableInt(Board_GPIO_GEREED);
    UART_PRINT("test interrupt gereed.\r\n");
    Gereed = true;
}

void gpioButtonInit(uint_least8_t index)//pin 61
{
    /*Init*/
//    GPIO_disableInt(Board_GPIO_INIT);
    UART_PRINT("test interrupt init.\r\n");
    GPIO_toggle(Board_GPIO_LED18);
    init = true;
}


void *mainThread(void *arg0)
{
    // Variabelen
        int Status;
        SlWlanSecParams_t SecParams; // Wifi parameters
        SlWlanSecParamsExt_t SecExtParams; // Wifi enterprise parameters
        _i8 ww[64]; // Buffer om een wifi-wachtwoord in op te slaan

        // Intialisaties
        sem_init(&wachtenOpIP, 0, 0); // Semaphore voor synchronisatie
        SPI_init();
        GPIO_init();
        InitTerm(); // UART functionaliteit van TI


        // Prio stack functie
        MaakTaak(9, 2048, sl_Task); // Nodig voor wifi api
        MaakTaak(2, 8192, App_Task); // Onze aplicatie

        /*----GPIO----*/
    GPIO_init();

    GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUTPUT);

    GPIO_setConfig(Board_GPIO_ROTARY_A, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setConfig(Board_GPIO_ROTARY_B, GPIO_CFG_IN_PU);
//    GPIO_setConfig(Board_GPIO_BUTTON16, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setConfig(Board_GPIO_STOP, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setConfig(Board_GPIO_GEREED, GPIO_CFG_IN_PD | GPIO_CFG_IN_INT_RISING);
    GPIO_setConfig(Board_GPIO_INIT, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setConfig(Board_GPIO_START, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);

    GPIO_setConfig(Board_GPIO_LED17, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(Board_GPIO_LED18, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);




    GPIO_setCallback(Board_GPIO_ROTARY_A, gpioButtonRotary);
//    GPIO_setCallback(Board_GPIO_ROTARY_B, gpioButtonFxn15);
    GPIO_setCallback(Board_GPIO_STOP, gpioButtonStop);
    GPIO_setCallback(Board_GPIO_INIT, gpioButtonInit);
    GPIO_setCallback(Board_GPIO_GEREED, gpioButtonGereed);
    GPIO_setCallback(Board_GPIO_START, gpioButtonStart);

    GPIO_enableInt(Board_GPIO_ROTARY_A);
    GPIO_enableInt(Board_GPIO_STOP);
   // GPIO_enableInt(Board_GPIO_GEREED);
    GPIO_enableInt(Board_GPIO_START);
    GPIO_enableInt(Board_GPIO_INIT);



    // Hello
    printf("\nApplicatie gestart.\nOverige meldingen zijn te vinden op de UART terminal op 115200 baud.\n");
    bool vraagOmWachtwoord = true; // Probeer eerst om opgeslagen wachtwoord te gebruiken
    do
    {
        // Wifi instellen en verbinden
        StelWifiIn(&SecParams, &SecExtParams, netwerkww, false);

        Status = sl_WlanConnect((_i8*)SSID, // SSID
                                strlen(SSID), // Lengte van SSID
                                0,
                                &SecParams, // Security parameters wifi
                                0); // Extended security parameters enterprice wifi
//        &SecExtParams
        if (Status !=0)
        {
            UART_PRINT("Wifi instellingen fout: %d\r\n", Status);
            vraagOmWachtwoord = true; // Probeer nogmaals om een correct wachtwoord in te voeren
        }
    }
    while (Status != 0);

    /*!
     * De thread kan nu stoppen.
     * Wachten op succesvolle verbinding zodat de App-thread
     * verder kan gaan.
     *
     * @see: SimpleLinkNetAppEventHandler(),
     * @see: App_Task(void * args)
     *
     */
    return 0;
}

void *App_Task(void *args)
{
    /*Phase 0*/

    _u32 EnkjelIP = SL_IPV4_VAL(192,168,43,57);
    _u32 Ivar = SL_IPV4_VAL(192,168,43,179);
    _u32 AndroidLPIP = SL_IPV4_VAL(192,168,43,57);
    _u16 len = sizeof(SlNetCfgIpV4Args_t);
    _u16 ConfigOpt = 0;
    _u32 googleIP = Ivar;
    SlNetCfgIpV4Args_t ipV4 = {0}; // Struct voor ip
    SlNetAppPingReport_t report; // Struct voor ping
    SlNetAppPingCommand_t pingCommand; // Struct voor ping

    /* Wachten op een ip adres, daarna kunnen we verder.
     * Zie ook SimpleLinkNetAppEventHandler() */
    sem_wait(&wachtenOpIP);

    // Wachtwoord alleen opslaan als verbinden gelukt is.
    schrijfWachtwoord();

    // Haal IP op
    sl_NetCfgGet(SL_NETCFG_IPV4_STA_ADDR_MODE,
                 &ConfigOpt,
                 &len,
                 (_u8 *)&ipV4
                );

    // Pretty print IP
    UART_PRINT( "\r\n"
                "Mijn IP\t %d.%d.%d.%d\r\n"
                "MASK\t %d.%d.%d.%d\r\n"
                "GW\t %d.%d.%d.%d\r\n"
                "DNS\t %d.%d.%d.%d\r\n",
                SL_IPV4_BYTE(ipV4.Ip,3), SL_IPV4_BYTE(ipV4.Ip,2), SL_IPV4_BYTE(ipV4.Ip,1), SL_IPV4_BYTE(ipV4.Ip,0),
                SL_IPV4_BYTE(ipV4.IpMask,3), SL_IPV4_BYTE(ipV4.IpMask,2), SL_IPV4_BYTE(ipV4.IpMask,1), SL_IPV4_BYTE(ipV4.IpMask,0),
                SL_IPV4_BYTE(ipV4.IpGateway,3), SL_IPV4_BYTE(ipV4.IpGateway,2), SL_IPV4_BYTE(ipV4.IpGateway,1), SL_IPV4_BYTE(ipV4.IpGateway,0),
                SL_IPV4_BYTE(ipV4.IpDnsServer,3), SL_IPV4_BYTE(ipV4.IpDnsServer,2), SL_IPV4_BYTE(ipV4.IpDnsServer,1), SL_IPV4_BYTE(ipV4.IpDnsServer,0));

    // DNS request naar google.nl, wat is het IP?
//    sl_NetAppDnsGetHostByName("www.google.nl", strlen("www.google.nl"), &googleIP, SL_AF_INET);

    //Wat willen we pingen?
    pingCommand.Ip = googleIP;
    pingCommand.PingSize = 32; // Size of ping, in bytes
    pingCommand.PingIntervalTime = 100; // Delay between pings, in milliseconds
    pingCommand.PingRequestTimeout = 1000; // Timeout for every ping in milliseconds
    pingCommand.TotalNumberOfAttempts = 4; // Max number of ping requests. 0 - forever
    pingCommand.Flags = 0; // Report only when finished

    // Feedback dat we starten met de ping operatie
    UART_PRINT("\r\nping naar mij (%d.%d.%d.%d)\r\n",
               SL_IPV4_BYTE(googleIP,3),
               SL_IPV4_BYTE(googleIP,2),
               SL_IPV4_BYTE(googleIP,1),
               SL_IPV4_BYTE(googleIP,0));


    // Ping uitvoeren
//    sl_NetAppPing(&pingCommand, SL_AF_INET, &report, PingResultaat);

    /*----CLIENT----*/
    _i16 handle;
    _i16 ret;
    char msg[25] = {NULL};
//    int MSG_LEN = 20;
//    char* message = "3";
    int i;
    int waitTime = 1;
    int z;
    int x;



    handle = sl_Socket(SL_AF_INET, SL_SOCK_STREAM, SL_IPPROTO_TCP);
    if(handle < 0){
        UART_PRINT("[SETUP][ERROR]Failed to setup socket:\t%d\r\n", handle);
        while(1);
    }
    UART_PRINT("Handle =\t%d\r\n", handle);

    SlSockAddrIn_t socketAddress;
    socketAddress.sin_family = SL_AF_INET;
    socketAddress.sin_port = sl_Htons(2000);
    socketAddress.sin_addr.s_addr = sl_Htonl(googleIP);

    handle = sl_Connect(handle, &socketAddress, sizeof(socketAddress));
    if(handle < 0){
        UART_PRINT("[SETUP][ERROR]Failed to setup connection:\t%d\r\n", handle);
        UART_PRINT("Right IP-address?\r\n");
        while(1);
    }
//    UART_PRINT("Wait 1 second...\r\n");
//    sleep(1);
    UART_PRINT("alright...Time to send stuff :D\r\n*************\r\n");

    while(1){
    /*----graph initialise----*/

        /*Phase 1*/
        UART_PRINT("Phase 1: Rotary encoder/User data\r\n");
        /*Rotary encoder sending*/
        char rotary[1] = {NULL};

//        while(init == false){
//            if(draaiknopstate == true){
//                /*sending*/
//                rotary[0] = draaiknop;
//                UART_PRINT("current message (type d):\t%d\r\n", msg);
//                UART_PRINT("current address (type d):\t%d\r\n", &msg);
//
//                ret = sl_Send(handle, &rotary, 1, NULL);
//                if(ret < 0){
//                    UART_PRINT("[SOCKET][ERROR]Message wasn't send:\t%d\r\n", ret);
//                    while(1);
//                }
//                //GPIO_toggle(Board_GPIO_LED2);
//                UART_PRINT("[SOCKET]Message was send(type s):\t'%s'\r\n", msg);
//                draaiknopstate = false;
//            }
//        }

        /*----Phase 2----*/
        UART_PRINT("Phase 2: start initialisatie\r\n");

        /*Start initialise*/
        char recv1[1] = {NULL};
        char recv2[140] = {NULL};

       // while(init==false){}       //---------------------------------------------------------------------------

        msg[0] = 'S';
        msg[1] = 'I';
        UART_PRINT("current message (type d):\t%d\r\n", msg);
        UART_PRINT("current address (type d):\t%d\r\n", &msg);

        ret = sl_Send(handle, &msg, 2, NULL);
        if(ret < 0){
            UART_PRINT("[SOCKET][ERROR]Message wasn't send:\t%d\r\n", ret);
            while(1);
        }

//        GPIO_toggle(Board_GPIO_LED2);
        UART_PRINT("[SOCKET]Message was send(type s):\t'%s'\r\n", msg);
        int parameters = 0;

        /*receive initialise value from python*/
        ret = sl_Recv(handle, &recv1, 7, NULL);
        if(ret < 0){
           UART_PRINT("[SOCKET][ERROR]Message wasn't received:\t%d\r\n", ret);
           while(1);
        }

        if (recv1[0] == 109){ /*m*/
           parameters = 32;
        }
        else if (recv1[0] == 112){ /*p*/
           parameters = 140;
       }

        ret = sl_Recv(handle, &recv2[0], parameters, NULL);

            if(ret < 0){

               UART_PRINT("[SOCKET][ERROR]Message wasn't received:\t%d\r\n", ret);
               while(1);
           }

            UART_PRINT("[Phase 2] Boodschap luidt:\r\n");
            for(i = 0 ; i < 140 ; i++){

                UART_PRINT("%s",recv2[i]);
            }

            UART_PRINT("\r\n");



            if (recv1[0] == 109){ /*m*/

                for(z = 0; z < 8; z++){

                    x = z*4;
                    manueel_arr[z] = ((asciiToInt(1,recv2[x])*1000)+(asciiToInt(1,recv2[(x+1)])*100)+(asciiToInt(1,recv2[(x+2)])*10)+(asciiToInt(1,recv2[(x+3)])));
                }

            }

            else if (recv1[0] == 112){ /*p*/


               for(z = 0; z < 35; z++){

                   x = z*4;
                   parameter_arr[z] = ((asciiToInt(1,recv2[x])*1000)+(asciiToInt(1,recv2[(x+1)])*100)+(asciiToInt(1,recv2[(x+2)])*10)+(asciiToInt(1,recv2[(x+3)])));
               }

            }



            int WE = ((asciiToInt(1,recv2[10])*10)+(asciiToInt(1,recv2[11])));

            UART_PRINT("[Phase 2] Wachten op gereed signaal deelsystemen...\r\n");

//           while(Gereed != true){}      //---------------------------------------------------------------------------------------

            UART_PRINT("[INIT]Alles gereed, startknop gereed\r\n");
            GPIO_write(Board_GPIO_LED17, Board_GPIO_LED_ON);

//           while(StartButton == false){}    //---------------------------------------------------------------------------------

           msg[0]= 'y';
           msg[1]= 'y';
           sl_Send(handle, &msg, 3, NULL);

           GPIO_write(Board_GPIO_LED18, Board_GPIO_LED_ON);
           UART_PRINT("[INIT]Start ingedrukt, we gaan beginnen\r\n");

           /*----Phase 3----*/
           /*sending from c to python*/
           ret = WarmteElement(WE); //set duty cycle
           if(ret<0){
               while(1);
           }

           int tijd = 0;
           char graph0[6];
           char graph1[6];
           char graph2[6];
           char graph3[6];
           char graph4[6];
           char graph5[6];
           char graph6[6];
           char graph7[6];
           char graph8[6];
           int snelheid_arr[30]={0, 20, 50, 100, 150, 200, 275, 350, 450, 600, 700, 800, 900, 1000, 1200, 1400, 1620, 1850, 2000, 2350, 2600, 3075, 3550, 4050, 4600, 5000, 5500, 5400, 3000, 200}; //snelheid
          int vbelasting_arr[30]={30, 32, 45, 46, 34, 38, 90, 56, 33, 12, 98, 112, 44, 45, 88, 95, 35, 12, 11, 10, 9, 8, 7, 6, 5, 77, 64, 37, 90, 66};
          int vaandrijving_arr[30]={67, 68, 98, 97, 96, 95, 94, 93, 92, 91, 5, 6, 8, 10, 96, 74, 32, 10, 112, 23, 14, 85, 34, 56, 82, 44, 56, 80, 12, 19};
          int rbelasting_arr[30]={91, 5, 6, 8, 10, 96, 74, 32, 10, 112, 50, 100, 150, 200, 275, 10, 112, 23, 14,67, 68, 98, 97, 96, 95, 94, 93, 92, 91, 5};
          int raandrijving_arr[30]={10, 96, 74, 32, 10, 112, 23, 14, 85, 34, 56, 82, 44, 56, 80, 12, 19, 67, 68, 98, 97, 96, 95, 94, 93, 92, 91, 5, 6, 8};
          int tempomgeving_arr[30]={21, 22, 21, 21, 21, 21, 21, 20, 17, 17, 17, 17, 18, 19, 27, 26, 25, 26, 27, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
          int tempbelasting_arr[30]={112, 23, 14, 85, 34, 56, 82, 44, 56, 80, 12, 19, 67, 68, 98, 97, 96, 95, 94, 93, 92, 91, 5, 6, 8, 10, 23, 27, 88, 99, 11};
          int koppel_arr[30]={100, 123, 150, 200, 210, 210, 210, 123, 150, 200, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 160, 150, 140, 120, 10};

            while(Stopknop==false){

                /*sending*/

                /*I2C RECEIVING*/
                /*
                 * char graph0[] = <tijd>;
                 * char graph1[] = <snelheid>;
                 * char graph2[] = <Vbelasting>;
                 * char graph3[] = <vaandrijving>;
                 * char graph4[] = <Rbelasting>;
                 * char graph5[] = <raandrijving>;
                 */

                sleep(waitTime);
                UART_PRINT("I2C data received: sending to python...\r\n");

                sprintf(graph0, "S%04d", tijd); //tijd
                sl_Send(handle, &graph0, sizeof(graph0), NULL);

                sprintf(graph1, "s%04d", snelheid_arr[tijd%30]);
                sprintf(graph2, "V%04d", vbelasting_arr[tijd%30]);
                sprintf(graph3, "v%04d", vaandrijving_arr[tijd%30]);
                sprintf(graph4, "R%04d", rbelasting_arr[tijd%30]);
                sprintf(graph5, "r%04d", raandrijving_arr[tijd%30]);
                sprintf(graph6, "t%04d", tempomgeving_arr[tijd%30]);
                sprintf(graph7, "T%04d", tempbelasting_arr[tijd%30]);
                sprintf(graph8, "K%04d", koppel_arr[tijd%30]);

                //UART_PRINT("test %s\r\n", graph1);

                sl_Send(handle, &graph1 , sizeof(graph1), NULL);
                sl_Send(handle, &graph2 , sizeof(graph2), NULL);
                sl_Send(handle, &graph3 , sizeof(graph3), NULL);
                sl_Send(handle, &graph4 , sizeof(graph4), NULL);
                sl_Send(handle, &graph5 , sizeof(graph5), NULL);
                sl_Send(handle, &graph6 , sizeof(graph6), NULL);
                sl_Send(handle, &graph7 , sizeof(graph7), NULL);
                sl_Send(handle, &graph8 , sizeof(graph8), NULL);

                tijd++;

    //                    if(tijd == 10){
    //                        Stopknop = true;
                }
            //    }

                msg[0]= 'z';
                msg[1]= 'z';
                sl_Send(handle, &msg, 3, NULL);
    //            sl_Close(handle);

                Stopknop = false;
                init = false;
                StartButton = false;
//                sleep(waitTime);
    }

    return NULL;
}

int asciiToInt(int i, char waarde){
    if(waarde != NULL && waarde != 'r' && waarde != 'g' && waarde != 'b' && waarde != 'c' && waarde != 'y'){
        waarde-= 48;
        return(waarde);
    }
}

char intToAscii(int i, int waarde){
    if(waarde != NULL && waarde != 'r' && waarde != 'g' && waarde != 'b' && waarde != 'c' && waarde != 'y'){
        waarde+= 48;
        return(waarde);
    }
}

// Callback van ping functie
void PingResultaat(SlNetAppPingReport_t* pReport)
{
    // Resultaten afdrukken.
    UART_PRINT("Verzonden: %d, "
               "Ontvangen: %d\r\n"
               "Gemiddelde Responsietijd: %dms\r\n",
               pReport->PacketsSent,
               pReport->PacketsReceived,
               pReport->AvgRoundTime);

    // Klaar met ping applicatie
}

void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args))
{
    pthread_t Thread;
    pthread_attr_t attrs;
    struct sched_param priParam;
    int retc;

    // sl_task thread maken. Dit handelt alle callbacks van wifi af
    priParam.sched_priority = prioriteit;
    retc = pthread_attr_init(&attrs);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED); // Ontkoppelde taak
    retc |= pthread_attr_setschedparam(&attrs, &priParam); // Prioriteit instellen
    retc |= pthread_attr_setstacksize(&attrs, stackSize); // Stacksize instellen

    retc |= pthread_create(&Thread, &attrs, functie, NULL); // Taak starten
    if (retc != 0)
    {
        // pthread_create() failed
        UART_PRINT("Taak fout: %d",retc);
        while (1);
    }
}

/*
 * WarmteElement
 * deze functie berekent de duty cycle aan de hand van een vooraf bepaalde functie
 * en zet hierna een PWM aan met de berekende duty cycle.
 *
 * RETURNS:      0 on success
 *              -1 PWM failed to initialize
 *              -2 PWM failed to open
 */
int WarmteElement(int P)
{
    int initWE = 0;
    static PWM_Handle PWMhandle;
    float D = 0;

    // Init PWM
    if(!initWE)
    {
        PWM_Params params;

        PWM_init();

        PWM_Params_init(&params);
        params.idleLevel = PWM_IDLE_LOW;      // Output low when PWM is not running
        params.periodUnits = PWM_PERIOD_HZ;   // Period is in Hz
        params.periodValue = 10000;           // 10kHz
        params.dutyUnits = PWM_DUTY_FRACTION; // Duty is fraction of period
        params.dutyValue = 0;                 // 0% duty cycle

        PWMhandle = PWM_open(PWM_Pin, &params);
        if (PWMhandle == NULL) {
            return -2;  //PWM failed to open
        }
        PWM_start(PWMhandle);
        initWE = true;
    }

        if(P > 25){P = 25;}

        //Calculate Duty-Cycle
        if(P <= 1.5){D = 16*sqrt(P);}
        else if(P < 8.6){D = 5.882 * P + 11.177;}
        else if(P < 19.4){D = 13.1 * sqrt(P - 6.6) + 40;}
        else if(P <= 25){D = 2.17 * P + 44.78;}

        //Catch special cases
        if(D > 99){D = 99;}
        else if(D < 0 || P == 0){D = 0 ;}

        D = D / 100;//blijkbaar werkt het alleen op een aparte regel

        PWM_setDuty(PWMhandle, PWM_DUTY_FRACTION_MAX * D);

    return 0;
}

int StelWifiIn(SlWlanSecParams_t* SecParams, SlWlanSecParamsExt_t* SecExtParams, _i8* wachtwoord, bool vraagOmWachtwoord)
{
    int Status;
    SlDateTime_t dateTime = {0};
    _u8 serverAuthenticatie = 0;

    // NWP starten en configureren
    sl_Start(0, 0, 0);
    Status = sl_WlanSetMode(ROLE_STA); // Station modus

    /* Om te voorkomen dat jouw wachtwoord in je code is te vinden wordt het
     * geschreven naar een bestand op de flash.
     *
     * Je moet eenmalig het wachtwoord geven, elke keer daarna kun je het
     * alleen uitlezen. Als je het wilt aanpassen dan moet parameter vraagOmWachtwoord
     * true worden.
     * */
//    leesWachtwoord((char *)wachtwoord, vraagOmWachtwoord);

    SecParams->Key = netwerkww;
    SecParams->KeyLen = strlen((const char*)wachtwoord);
    SecParams->Type = SL_WLAN_SEC_TYPE_WPA_WPA2;

    // extended alleen nodig voor enterprise wifi
    SecExtParams->User = IDENTITY;
    SecExtParams->UserLen = strlen(IDENTITY);
    SecExtParams->AnonUser = 0;
    SecExtParams->AnonUserLen = 0;
    SecExtParams->EapMethod = SL_WLAN_ENT_EAP_METHOD_PEAP0_MSCHAPv2;

    // Jaar instellen zodat het certificaat niet wordt afgekeurd
    dateTime.tm_year =  (_u32)2019; // Year (YYYY format)

    // NWP herstarten om STA modus ook te gebruiken
    sl_Stop(0);
    sl_Start(0, 0, 0);

    // Tijd opslaan
    sl_DeviceSet(SL_DEVICE_GENERAL,
                 SL_DEVICE_GENERAL_DATE_TIME,
                 sizeof(SlDateTime_t),
                 (_u8 *)(&dateTime));

    // Eduroam geeft geen CA certificaat uit, dus check uitzetten
    Status = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, SL_WLAN_GENERAL_PARAM_DISABLE_ENT_SERVER_AUTH, 1, &serverAuthenticatie);
    if(Status == 0){
        UART_PRINT("Connection successful-ish\r\n");
        UART_PRINT("If it stops here: check password configs or\r\n"
                    "the network is still up");
    }

    return Status;
}
