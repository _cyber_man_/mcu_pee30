from tkinter import *
import time
import threading
import numpy as np
import pylab as pl
import matplotlib.pyplot as plt
import math
import socket
import struct


#192.168.43.179

HOST = '0.0.0.0' # accept any IP-adres
PORT = 2000 # Port to listen on
char = ''
#buffer = bytearray(b'')
buffer = []
conn = []

root = Tk()

co_0 = 19
co_1 = co_0+1
co_2 = co_0+2
co_3 = co_0+3
co_4 = co_0+4

ro_1 = 1
ro_2 = ro_1 + 1
ro_3 = ro_1+2
ro_4 = ro_1+3
ro_5 = ro_1+4
ro_6 = ro_1+5
ro_7 = ro_1+6
ro_8 = ro_1+7
ro_9 = ro_1+8
ro_10 = ro_1+9

procheck = 6

maxro_1 = 10
maxro_2 = maxro_1 +1
maxro_3 = maxro_1 +2
maxro_4 = maxro_1 +3
maxro_5 = maxro_1 +4

submitbutton = maxro_5 + 2

prowa_1 = ["0001", "0002", "0003", "0004", "0005", "0006", "0007", "0008", "0009", "0010"]
#prowa_1 = ["0001", "0002", "0005", "0010", "0010", "0020", "0020", "0020", "0020", "0020"]
prowa_2 = ["0011", "0012", "0013", "0014", "0015", "0016", "0017", "0018", "0019", "0020"] #test
#prowa_2 = ["0100", "0200", "0400", "0500", "0750", "1000", "2000", "3000", "4000", "5000"]
prowa_3 = ["0021", "0022", "0023", "0024", "0025", "0026", "0027", "0028", "0029", "0030"] #test
#prowa_3 = ["5", "10", "12", "14", "16", "18", "20", "25", "10", "10"]

counter = 0
nummer = 0


startgrafiek = 0


#snelheid = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#vbelasting = [0, 40, 100, 120, 110, 111, 140, 230, 200, 170, 200]
#vaandrijving = [0, 300, 200, 220, 240, 260, 280, 300, 320, 340, 360]
#rbelasting = [0, 400, 390, 450, 360, 320, 400, 530, 430, 330, 360]
#raandrijving = [0, 600, 530, 430, 603, 204, 300, 400, 340, 450, 460]
tijd = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
snelheid = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
vbelasting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
vaandrijving = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
rbelasting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
raandrijving = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#tijd = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
tomgeving = 0
tbelasting = 0
koppel = 0
STOP = 0

#snelheidn = []
#vbelastingn = []
#vaandrijvingn = []
#rbelastingn = []
#raandrijvingn = []

def verschuiving(array, waarde): #neemt de array die je wilt aanpassen, haalt de eerst weg, schuift alles 1 naar links en vervangt de laatste met waarde en returnt het
    for i in range(1, len(array)):
        array[i - 1] = array[i]
    array[len(array)-1] = waarde
    return array
        

def grafiekPlot():
    global STOP, tijd, snelheid, vbelasting, vaandrijving, rbelasting, raandrijving, tomgeving, tbelasting, koppel
    global startgrafiek
    while(1):          
        if(startgrafiek == 1): #de grafiek code wordt pas gestart zodra er op de submit knop is gedrukt
            plt.figure(figsize=(12,8))
            print("hoi ben ik weer")
            
            
            while(1):
                    
                plt.cla()
                plt.title("Gemeten waarden")
                psnelheid = plt.plot(tijd, snelheid, "r", label="Snelheid - RPM")
                pvbelasting = plt.plot(tijd, vbelasting, "g", label="Vermogen belasting - W")
                pvaandrijving = plt.plot(tijd, vaandrijving,"b", label="Vermogen aandrijving - W")
                prbelasting = plt.plot(tijd, rbelasting,"c", label="Rendement belasting - %")
                praandrijving = plt.plot(tijd, raandrijving,"y", label="Rendement aandrijving - %")
                plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
                plt.xlabel("Tijd (s)")
                plt.tight_layout()
                plt.pause(1)
                
                if(STOP==1):
                    
                    plt.close()
                    tijd = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
                    snelheid = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    vbelasting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    vaandrijving = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    rbelasting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    raandrijving = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    tomgeving = 0
                    tbelasting = 0
                    koppel = 0
                    STOP = 0
                    startgrafiek = 0
                    break
                
            

        
def receiver():
    global STOP, tijd, snelheid, vbelasting, vaandrijving, rbelasting, raandrijving, tomgeving, tbelasting, koppel
    counter = 0
    global conn
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen(1)
        print('listening: ', (HOST, PORT))
        conn, addr = s.accept()
        print(conn)
        connection.config(state=NORMAL)
        connection.select()
        connection.config(state=DISABLED)
        waarde = []
        
        while(1):
        
            while(STOP==0):
                
                with conn:
                    
                    while True:
                        
                        buffer = conn.recv(4096)
                        if not buffer:
                            break
                        print(len(buffer))
                        print(buffer)
                        print(buffer.decode('Cp1252'))
                        waarde = str(buffer.decode('Cp1252'))
                        global startgrafiek

                        
                        if (startgrafiek==0):
                        
                            if (waarde == 'SI'):
                                submitSettings()
                                print("huh?")
                                
                            elif (len(buffer)==2):
                                probeer = (buffer[0] | buffer[1]<<8 )
                                print(probeer)
                                rpm.config(text=str(probeer))
                                inv_2.delete(first=0, last=10)
                                inv_2.insert(0, str(probeer))
                                
                            elif (len(buffer)==1):
                                probeer = (buffer[0])
                                print(probeer)
                                rpm.config(text=str(probeer))
                                inv_2.delete(first=0, last=10)
                                inv_2.insert(0, str(probeer))
                                
                            elif (len(buffer)==3):
                                probeer = (buffer[0] | buffer[1]<<8 | buffer[2]<<16)
                                print(probeer)
                                rpm.config(text=str(probeer))
                                inv_2.delete(first=0, last=10)
                                inv_2.insert(0, str(probeer))
                                
                            elif (len(buffer)==4):
                                probeer = (buffer[0] | buffer[1]<<8 | buffer[2]<<16 | buffer[3]<<24)
                                print(probeer)
                                rpm.config(text=str(probeer))
                                inv_2.delete(first=0, last=10)
                                inv_2.insert(0, str(probeer))
                                
                                                          
                        elif(startgrafiek==1):
                            
                            if(len(buffer) != 1):
                                print(waarde)
    #                            print("blub")
                                words = waarde.split("\x00")
                                aantal = len(words)-1
                                print(str(words))
                                
                                for i in range (0, aantal):
                                    
                                    if(words[i][0] == 's'):
                                        snelheidn = words[i]
                                        verschuiving(snelheid, int(snelheidn[1:]))
                                        
                                    elif(words[i][0] == 'V'):
                                        vbelastingn = words[i]
                                        verschuiving(vbelasting, int(vbelastingn[1:]))
                                        
                                    elif(words[i][0] == 'v'):
                                        vaandrijvingn = words[i]
                                        verschuiving(vaandrijving, int(vaandrijvingn[1:]))
                                        
                                    elif(words[i][0] == 'R'):
                                        rbelastingn = words[i]
                                        idk = verschuiving(rbelasting, int(rbelastingn[1:]))
                                        
                                    elif(words[i][0] == 'r'):
                                        raandrijvingn = words[i]
                                        verschuiving(raandrijving, int(raandrijvingn[1:]))
                                        
                                    elif(words[i][0] == 'S'):
                                        tijdn = words[i]
                                        verschuiving(tijd, int(tijdn[1:]))
                                        
                                    elif(words[i][0] == 't'):
                                        tomgevingn = words[i]
                                        global tomgeving
                                        tomgeving = int(tomgevingn[1:])
                                        Omgevingtemp.config(text=str(tomgeving))
                                        
                                    elif(words[i][0] == 'T'):
                                        tbelastingn = words[i]
                                        global tbelasting
                                        tbelasting = int(tbelastingn[1:])
                                        Belastingtemp.config(text=str(tbelasting))
                                        
                                    elif(words[i][0] == 'K'):
                                        koppeln = words[i]
                                        global koppel
                                        koppel = int(koppeln[1:])
                                        KOPPEL.config(text=str(koppel))
                                   
                                        
                                    elif(words[i][0] == 'y'):
                                        #als op start
                                        print("start")
                                        startit.config(state=NORMAL)
                                        startit.select()
                                        startit.config(state=DISABLED)
                                        
                                    elif(words[i][0] == 'z'):
                                        #global startgrafiek
                                        #global STOP
                                        
                                        startgrafiek = 0
                                        STOP = 1
                                        startit.config(state=NORMAL)
                                        startit.deselect()
                                        startit.config(state=DISABLED)
                                        startinit.config(state=NORMAL)
                                        startinit.deselect()
                                        startinit.config(state=DISABLED)
                                        stopit.config(state=NORMAL)
                                        stopit.select()
                                        stopit.config(state=DISABLED)
                                        
                                        #global STOP
                                        
                    
                    connection.config(state=NORMAL)
                    connection.deselect()
                    connection.config(state=DISABLED)
            
            #global tijd, snelheid, vbelasting, vaandrijving, rbelasting, raandrijving, tomgeving, tbelasting, koppel
            tijd = [-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
            snelheid = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            vbelasting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            vaandrijving = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            rbelasting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            raandrijving = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            tomgeving = 0
            tbelasting = 0
            koppel = 0
            
            STOP = 0


def submitSettings(): #hier worden alle gegevens verzameld voor om te versturen.
    
    global startgrafiek, STOP
    STOP = 0
    startgrafiek = 1
    global conn
    print(conn)
    print ("hij doet het shithead", inv_1.get())
    startinit.config(state=NORMAL)
    startinit.select
    startinit.config(state=DISABLED)
    
    if(manueel.get()==1):
        
        if (pro_1.get()==1):
            
            warmte = [str('{:04d}'.format(int(propar_31.get()))), str('{:04d}'.format(int(propar_32.get()))), str('{:04d}'.format(int(propar_33.get()))), str('{:04d}'.format(int(propar_34.get()))), str('{:04d}'.format(int(propar_35.get()))), str('{:04d}'.format(int(propar_36.get()))), str('{:04d}'.format(int(propar_37.get()))), str('{:04d}'.format(int(propar_38.get()))), str('{:04d}'.format(int(propar_39.get()))), str('{:04d}'.format(int(propar_40.get())))]
            maxpro = [str('{:04d}'.format(int(inv_4.get()))), str('{:04d}'.format(int(inv_5.get()))), str('{:04d}'.format(int(inv_6.get()))), str('{:04d}'.format(int(inv_7.get()))), str('{:04d}'.format(int(inv_8.get())))]
            
            bprowa_2 = []
            bwarmte = []
            bmaxpro = []
            
            verzendletter = "p"
            bprowa_1 = [verzendletter.encode()]
            
            for i in range(0, (len(prowa_1))):
                bprowa_1.append((prowa_1[i]).encode())
                
            for i in range(0, (len(prowa_2))):
                bprowa_2.append((prowa_2[i]).encode())
                
            for i in range(0, (len(warmte))):
                bwarmte.append((warmte[i]).encode())
                
            for i in range(0, (len(maxpro))):
                bmaxpro.append((maxpro[i]).encode())
                
            bdata = bprowa_1.extend(bprowa_2)
            bdata = bprowa_1.extend(bwarmte)
            bdata = bprowa_1.extend(bmaxpro)
            
            data = [prowa_1, prowa_2, warmte, maxpro]
            
            for i in range(0, len(bprowa_1)):
                conn.send(bprowa_1[i])
        
    else:
        
        invoer = [str('{:04d}'.format(int(inv_1.get()))), str('{:04d}'.format(int(inv_2.get()))), str('{:04d}'.format(int(inv_3.get())))]
        verzendletter = "m"
        binvoer = [verzendletter.encode()]
        #invoer = [ str(1111), str(2222), str(3333)] #test waarden
        
        for i in range(0, (len(invoer))):
            #binvoer[i] = (invoer[i]).encode()
            binvoer.append((invoer[i]).encode())
        
        print(str(binvoer))
        maxpro = [str(inv_4.get()), str(inv_5.get()), str(inv_6.get()), str(inv_7.get()), str(inv_8.get())]
        bmaxpro = []
        #maxpro = [str(4444), str(5555), str(9999), str(7777), str(8888)] #test waarden
        
        for i in range(0, (len(maxpro))):
            bmaxpro.append((maxpro[i]).encode())
            
        #data = [invoer, maxpro]
        data = invoer.extend(maxpro)
        bdata = binvoer.extend(bmaxpro)
        print (str(data)) #hier kan de data gestuurd worden
        print (str(binvoer))
            
        for i in range(0, len(binvoer)):
            conn.send(binvoer[i])
        
        
        
        #conn.send()
        print('Message was send: '+ str(binvoer))


def printInstelling(): #enablet of disablet de invoer van bepaalde instellingen bij het kiezen van manueel of profiel.
    print ("Manueel", manueel.get())
    
    if (manueel.get()==1):
        inv_1.config(state=DISABLED)
        inv_2.config(state=DISABLED)
        inv_3.config(state=DISABLED)
        rpm.config(state=DISABLED)
        profiel_1.config(state=NORMAL)
        profiel_2.config(state=NORMAL)
        profiel_3.config(state=NORMAL)
        propar_31.config(state=NORMAL)
        propar_32.config(state=NORMAL)
        propar_33.config(state=NORMAL)
        propar_34.config(state=NORMAL)
        propar_35.config(state=NORMAL)
        propar_36.config(state=NORMAL)
        propar_37.config(state=NORMAL)
        propar_38.config(state=NORMAL)
        propar_39.config(state=NORMAL)
        propar_40.config(state=NORMAL)

    else:
        inv_1.config(state=NORMAL)
        inv_2.config(state=NORMAL)
        inv_3.config(state=NORMAL)
        rpm.config(state=NORMAL)
        profiel_1.config(state=DISABLED)
        profiel_2.config(state=DISABLED)
        profiel_3.config(state=DISABLED)
        propar_31.config(state=DISABLED)
        propar_32.config(state=DISABLED)
        propar_33.config(state=DISABLED)
        propar_34.config(state=DISABLED)
        propar_35.config(state=DISABLED)
        propar_36.config(state=DISABLED)
        propar_37.config(state=DISABLED)
        propar_38.config(state=DISABLED)
        propar_39.config(state=DISABLED)
        propar_40.config(state=DISABLED)

        
def vinker_1(): #als 1 profiel wordt gekozen worden alle profielen automatisch gekozen
    
    if(pro_1.get()==1):
        profiel_2.select()
        profiel_3.select()
        
    else:
        profiel_2.deselect()
        profiel_3.deselect()
       
    
def fillMaxIn(): #standaard waarden zodat je niet zo lang hoeft te doen over het invoeren bij het testen
#    inv_4.insert(0, "stroom")
#    inv_5.insert(0, "spanning")
#    inv_6.insert(0, "vermogen")
#    inv_7.insert(0, "snelheid")
#    inv_8.insert(0, "temperatuur")
    inv_1.insert(0, "0060") #aanbevolen vermogen
    inv_2.insert(0, "3000") #aanbevolen snelheid
    inv_3.insert(0, "0005") #aanbevolen temperatuur
    inv_4.insert(0, "0003") #aanbevolen max stroom
    inv_5.insert(0, "0030") #aanbevolen max spanning
    inv_6.insert(0, "0060") #aanbevolen max vermogen
    inv_7.insert(0, "7000") #aanbevolen max snelheid
    inv_8.insert(0, "0050") #aanbevolen max temperatuur



   
manueel = IntVar()
pro_1 = IntVar()
pro_2 = IntVar()
pro_3 = IntVar()
pro_4 = IntVar()
verbonden = IntVar()
Startinit = IntVar()
Startit = IntVar()
Stopit = IntVar()

inst_1 = Label(root, text="Vermogen Belasting Setpoint", width=30, anchor=W, font="LucidaConsole")
inst_2 = Label(root, text="Snelheid Motor", width=30, anchor=W, font="LucidaConsole")
inst_3 = Label(root, text="Vermogen Warmte Element", width=30, anchor=W, font="LucidaConsole")
inst_4 = Label(root, text="Max. stroom aandrijving", width=30, anchor=W, font="LucidaConsole")
inst_5 = Label(root, text="Max. spanning aandrijving", width=30, anchor=W, font="LucidaConsole")
inst_6 = Label(root, text="Max. vermogen belasting", width=30, anchor=W, font="LucidaConsole")
inst_7 = Label(root, text="Max. snelheid:", width=30, anchor=W, font="LucidaConsole")
inst_8 = Label(root, text="Max. temperatuur belasting", width=30, anchor=W, font="LucidaConsole")

netheid_1 = Label(root, text=":", width=5, anchor=E)
netheid_2 = Label(root, text=":", width=5, anchor=E)
netheid_3 = Label(root, text=":", width=5, anchor=E)
netheid_4 = Label(root, text=":", width=5, anchor=E)
netheid_5 = Label(root, text=":", width=5, anchor=E)
netheid_6 = Label(root, text=":", width=5, anchor=E)
netheid_7 = Label(root, text=":", width=5, anchor=E)
netheid_8 = Label(root, text=":", width=5, anchor=E)

netheid_10 = Label(root, text=":", width=1, anchor=E)
netheid_11 = Label(root, text=":", width=1, anchor=E)
netheid_12 = Label(root, text=":", width=1, anchor=E)

tussen_1 = Label(root, text=" ")
tussen_2 = Label(root, text=" ")
tussen_3 = Label(root, text=" ")
tussen_4 = Label(root, text=" ")
titel_1 = Label(root, text="Veiligheids parameters:", font=("Helvetica", 20))
titel_0 = Label(root, text="SetPoints:", font=("Helvetica", 20))

titel_7 = Label(root, text="Profiel 1: \n Vermogen Belasting", width=15)
titel_8 = Label(root, text="Profiel 2: \n Snelheid Motor", width=15)
titel_9 = Label(root, text="Profiel 3: \n Verwarm Vermogen", width=15)


unit_1 = Label(root, text="Watt", width=10, anchor=W)
unit_2 = Label(root, text="RPM", width=10, anchor=W)
unit_3 = Label(root, text="Watt", width=10, anchor=W)
unit_4 = Label(root, text="Ampere", width=10, anchor=W)
unit_5 = Label(root, text="Volt", width=10, anchor=W)
unit_6 = Label(root, text="Watt", width=10, anchor=W)
unit_7 = Label(root, text="RPM", width=10, anchor=W)
unit_8 = Label(root, text="Graden °C", width=10, anchor=W)
unit_9 = Label(root, text="RPM waarde:", width=10)

unit_10 = Label(root, text="Graden °C", width=15, anchor=W)
unit_11 = Label(root, text="Graden °C", width=15, anchor=W)
unit_12 = Label(root, text="Newton meter", width=15, anchor=W)


dubbel_1 = Label(root, text="Waarde 1:", width=15, anchor=E)
dubbel_2 = Label(root, text="Waarde 2:", width=15, anchor=E)
dubbel_3 = Label(root, text="Waarde 3:", width=15, anchor=E)
dubbel_4 = Label(root, text="Waarde 4:", width=15, anchor=E)
dubbel_5 = Label(root, text="Waarde 5:", width=15, anchor=E)
dubbel_6 = Label(root, text="Waarde 6:", width=15, anchor=E)
dubbel_7 = Label(root, text="Waarde 7:", width=15, anchor=E)
dubbel_8 = Label(root, text="Waarde 8:", width=15, anchor=E)
dubbel_9 = Label(root, text="Waarde 9:", width=15, anchor=E)
dubbel_10 = Label(root, text="Waarde 10:", width=15, anchor=E)

rpm = Label(root, text="test", font=("ComicSans",50), width=5)
omgevingtemp = Label(root, text="Temperatuur Omgeving:", width=18, anchor=W)
belastingtemp = Label(root, text="Temperatuur Belasting:", width=18, anchor=W)
Koppel = Label(root, text="Koppel:", width=18, anchor=W)

Omgevingtemp = Label(root, text="0", width=4)
Belastingtemp = Label(root, text="0", width=4)
KOPPEL = Label(root, text="0", width=4)

#rpm_test(rpm)

inv_1 = Entry(root)
inv_2 = Entry(root)
inv_3 = Entry(root)
inv_4 = Entry(root)
inv_5 = Entry(root)
inv_6 = Entry(root)
inv_7 = Entry(root)
inv_8 = Entry(root)


propar_1 = Entry(root)
propar_2 = Entry(root)
propar_3 = Entry(root)
propar_4 = Entry(root)
propar_5 = Entry(root)
propar_6 = Entry(root)
propar_7 = Entry(root)
propar_8 = Entry(root)
propar_9 = Entry(root)
propar_10 = Entry(root)

propar_16 = Entry(root)
propar_17 = Entry(root)
propar_18 = Entry(root)
propar_19 = Entry(root)
propar_20 = Entry(root)
propar_21 = Entry(root)
propar_22 = Entry(root)
propar_23 = Entry(root)
propar_24 = Entry(root)
propar_25 = Entry(root)

propar_31 = Entry(root)
propar_32 = Entry(root)
propar_33 = Entry(root)
propar_34 = Entry(root)
propar_35 = Entry(root)
propar_36 = Entry(root)
propar_37 = Entry(root)
propar_38 = Entry(root)
propar_39 = Entry(root)
propar_40 = Entry(root)


c = Checkbutton(root, text="Manueel", command=printInstelling, variable=manueel, onvalue=0, offvalue=1, width=20, anchor=CENTER)
s = Button(root, text="Submit", command=submitSettings, width=20)
recommended = Button(root, text="Standaard Instellingen", command=fillMaxIn, width=20)

profiel_1 = Checkbutton(root, text="Profiel 1", variable=pro_1, state=DISABLED, onvalue=1, offvalue=0, command=vinker_1)
profiel_2 = Checkbutton(root, text="Profiel 2", variable=pro_1, state=DISABLED, command=vinker_1)
profiel_3 = Checkbutton(root, text="Profiel 3", variable=pro_1, state=DISABLED, command=vinker_1)

connection = Checkbutton(root, text="Connectie", variable=verbonden, state=DISABLED, anchor=CENTER)
startinit = Checkbutton(root, text="Start Init", variable=Startinit, state=DISABLED, anchor=CENTER)
startit = Checkbutton(root, text="Start", variable=Startit, state=DISABLED, anchor=W)
stopit = Checkbutton(root, text="Stop", variable=Stopit, state=DISABLED, anchor=E)

inst_1.grid(row=1, columnspan=4)
inst_2.grid(row=2, columnspan=4)
inst_3.grid(row=3, columnspan=4)
inst_4.grid(row=maxro_1, columnspan=4)
inst_5.grid(row=maxro_2, columnspan=4)
inst_6.grid(row=maxro_3, columnspan=4)
inst_7.grid(row=maxro_4, columnspan=4)
inst_8.grid(row=maxro_5, columnspan=4)

inv_1.grid(row=1, column=12)
inv_3.grid(row=3, column=12)
inv_2.grid(row=2, column=12)
inv_4.grid(row=maxro_1, column=12)
inv_5.grid(row=maxro_2, column=12)
inv_6.grid(row=maxro_3, column=12)
inv_7.grid(row=maxro_4, column=12)
inv_8.grid(row=maxro_5, column=12)

propar_1.grid(row=ro_1, column=co_1)
propar_2.grid(row=ro_2, column=co_1)
propar_3.grid(row=ro_3, column=co_1)
propar_4.grid(row=ro_4, column=co_1)
propar_5.grid(row=ro_5, column=co_1)
propar_6.grid(row=ro_6, column=co_1)
propar_7.grid(row=ro_7, column=co_1)
propar_8.grid(row=ro_8, column=co_1)
propar_9.grid(row=ro_9, column=co_1)
propar_10.grid(row=ro_10, column=co_1)

propar_16.grid(row=ro_1, column=co_2)
propar_17.grid(row=ro_2, column=co_2)
propar_18.grid(row=ro_3, column=co_2)
propar_19.grid(row=ro_4, column=co_2)
propar_20.grid(row=ro_5, column=co_2)
propar_21.grid(row=ro_6, column=co_2)
propar_22.grid(row=ro_7, column=co_2)
propar_23.grid(row=ro_8, column=co_2)
propar_24.grid(row=ro_9, column=co_2)
propar_25.grid(row=ro_10, column=co_2)

propar_31.grid(row=ro_1, column=co_3)
propar_32.grid(row=ro_2, column=co_3)
propar_33.grid(row=ro_3, column=co_3)
propar_34.grid(row=ro_4, column=co_3)
propar_35.grid(row=ro_5, column=co_3)
propar_36.grid(row=ro_6, column=co_3)
propar_37.grid(row=ro_7, column=co_3)
propar_38.grid(row=ro_8, column=co_3)
propar_39.grid(row=ro_9, column=co_3)
propar_40.grid(row=ro_10, column=co_3)

netheid_1.grid(row=1, column=5)
netheid_2.grid(row=2, column=5)
netheid_3.grid(row=3, column=5)

netheid_4.grid(row=maxro_1, column=5)
netheid_5.grid(row=maxro_2, column=5)
netheid_6.grid(row=maxro_3, column=5)
netheid_7.grid(row=maxro_4, column=5)
netheid_8.grid(row=maxro_5, column=5)

netheid_10.grid(row=maxro_3, column=15)
netheid_11.grid(row=maxro_4, column=15)
netheid_12.grid(row=maxro_5, column=15)

unit_1.grid(row=1, column=13)
unit_2.grid(row=2, column=13)
unit_3.grid(row=3, column=13)
unit_4.grid(row=maxro_1, column=13)
unit_5.grid(row=maxro_2, column=13)
unit_6.grid(row=maxro_3, column=13)
unit_7.grid(row=maxro_4, column=13)
unit_8.grid(row=maxro_5, column=13)
unit_9.grid(row=4, column=14, columnspan=2)

unit_10.grid(row=maxro_3, column=19, columnspan=1)
unit_11.grid(row=maxro_4, column=19, columnspan=1)
unit_12.grid(row=maxro_5, column=19, columnspan=1)

dubbel_1.grid(row=ro_1, column=19)
dubbel_2.grid(row=ro_2, column=19)
dubbel_3.grid(row=ro_3, column=19)
dubbel_4.grid(row=ro_4, column=19)
dubbel_5.grid(row=ro_5, column=19)
dubbel_6.grid(row=ro_6, column=19)
dubbel_7.grid(row=ro_7, column=19)
dubbel_8.grid(row=ro_8, column=19)
dubbel_9.grid(row=ro_9, column=19)
dubbel_10.grid(row=ro_10, column=19)

titel_1.grid(columnspan=5, row=8)
titel_0.grid(columnspan=5, row=0)

titel_7.grid(row=0, column=20)
titel_8.grid(row=0, column=21)
titel_9.grid(row=0, column=22)

rpm.grid(column=14, columnspan=1, row=1, rowspan=3)
omgevingtemp.grid(column=14, columnspan=1, row=maxro_3, rowspan=1)
belastingtemp.grid(column=14, columnspan=1, row=maxro_4, rowspan=1)
Koppel.grid(column=14, columnspan=1, row=maxro_5, rowspan=1)
Omgevingtemp.grid(column=16, columnspan=1, row=maxro_3, rowspan=1)
Belastingtemp.grid(column=16, columnspan=1, row=maxro_4, rowspan=1)
KOPPEL.grid(column=16, columnspan=1, row=maxro_5, rowspan=1)

c.grid(row=procheck)
s.grid(columnspan=6, row=submitbutton, rowspan=2)
recommended.grid(row=8, column=12)
profiel_1.grid(column=3, row=procheck)
profiel_2.grid(column=4, row=procheck)
profiel_3.grid(column=5, row=procheck)

connection.grid(column=14, row=procheck)
startinit.grid(column=14, row=(1+procheck))
startit.grid(column=14, row=(2+procheck))
stopit.grid(column=14, row=(3+procheck))


propar_1.insert(0, str(prowa_1[0]))
propar_2.insert(0, str(prowa_1[1]))
propar_3.insert(0, str(prowa_1[2]))
propar_4.insert(0, str(prowa_1[3]))
propar_5.insert(0, str(prowa_1[4]))
propar_6.insert(0, str(prowa_1[5]))
propar_7.insert(0, str(prowa_1[6]))
propar_8.insert(0, str(prowa_1[7]))
propar_9.insert(0, str(prowa_1[8]))
propar_10.insert(0, str(prowa_1[9]))

propar_16.insert(0, str(prowa_2[0]))
propar_17.insert(0, str(prowa_2[1]))
propar_18.insert(0, str(prowa_2[2]))
propar_19.insert(0, str(prowa_2[3]))
propar_20.insert(0, str(prowa_2[4]))
propar_21.insert(0, str(prowa_2[5]))
propar_22.insert(0, str(prowa_2[6]))
propar_23.insert(0, str(prowa_2[7]))
propar_24.insert(0, str(prowa_2[8]))
propar_25.insert(0, str(prowa_2[9]))

propar_31.insert(0, str(prowa_3[0]))
propar_32.insert(0, str(prowa_3[1]))
propar_33.insert(0, str(prowa_3[2]))
propar_34.insert(0, str(prowa_3[3]))
propar_35.insert(0, str(prowa_3[4]))
propar_36.insert(0, str(prowa_3[5]))
propar_37.insert(0, str(prowa_3[6]))
propar_38.insert(0, str(prowa_3[7]))
propar_39.insert(0, str(prowa_3[8]))
propar_40.insert(0, str(prowa_3[9]))


propar_1.config(state=DISABLED)
propar_2.config(state=DISABLED)
propar_3.config(state=DISABLED)
propar_4.config(state=DISABLED)
propar_5.config(state=DISABLED)
propar_6.config(state=DISABLED)
propar_7.config(state=DISABLED)
propar_8.config(state=DISABLED)
propar_9.config(state=DISABLED)

propar_10.config(state=DISABLED)
propar_16.config(state=DISABLED)
propar_17.config(state=DISABLED)
propar_18.config(state=DISABLED)
propar_19.config(state=DISABLED)
propar_20.config(state=DISABLED)
propar_21.config(state=DISABLED)
propar_22.config(state=DISABLED)
propar_23.config(state=DISABLED)
propar_24.config(state=DISABLED)
propar_25.config(state=DISABLED)

propar_31.config(state=DISABLED)
propar_32.config(state=DISABLED)
propar_33.config(state=DISABLED)
propar_34.config(state=DISABLED)
propar_35.config(state=DISABLED)
propar_36.config(state=DISABLED)
propar_37.config(state=DISABLED)
propar_38.config(state=DISABLED)
propar_39.config(state=DISABLED)
propar_40.config(state=DISABLED)

t1 = threading.Thread(target=grafiekPlot)
t2 = threading.Thread(target=receiver)
t1.start()
t2.start()

print("tadaaa")

root.mainloop()