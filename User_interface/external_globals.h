/*
 *  external_globals.h
 *
 *  Created on: 12 dec. 2019
 *  Author: 0904269
 *
 */

#ifndef EXTERNAL_GLOBALS_H_
#define EXTERNAL_GLOBALS_H_

/* DEFINES */
#define G_BUFFER_SIZE_EXT       8

/* GLOBALS */
extern char g_InitParameters[G_BUFFER_SIZE_EXT];

#endif /* EXTERNAL_GLOBALS_H_ */
