/*
 * Copyright (c) 2015-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== pwmled2.c ========
 */
/* For usleep() */
#include <unistd.h>
#include <stddef.h>
#include <stdbool.h>
#include <math.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/gpio/GPIOCC32XX.h>
#include <ti/drivers/PWM.h>
#include <ti/drivers/pwm/PWMTimerCC32XX.h>

/* Example/Board Header files */
#include "Board.h"

#define PWM_Pin     Board_PWM0

/*
 * PWM_WarmteElement
 * deze functie berekent de duty cycle aan de hand van een vooraf bepaalde functie
 * en zet hierna een PWM aan met de berekende duty cycle.
 *
 * RETURNS:      0 on success
 *              -1 PWM failed to initialize
 *              -2 PWM failed to open
 */
int WarmteElement(int P)
{
    static int initWE = 0;
    static PWM_Handle handle;
    float D = 0;

    // Init PWM
    if(!initWE)
    {
        PWM_Params params;

        PWM_init();

        PWM_Params_init(&params);
        params.idleLevel = PWM_IDLE_LOW;      // Output low when PWM is not running
        params.periodUnits = PWM_PERIOD_HZ;   // Period is in Hz
        params.periodValue = 10000;           // 10kHz
        params.dutyUnits = PWM_DUTY_FRACTION; // Duty is fraction of period
        params.dutyValue = 0;                 // 0% duty cycle

        handle = PWM_open(PWM_Pin, &params);
        if (handle == NULL) {
            return -2;  //PWM failed to open
        }
        PWM_start(handle);
        initWE = true;

        //Calculate Duty-Cycle
        if(P <= 1.5){D = 16*sqrt(P);}
        else if(P < 8.6){D = 5.882 * P + 11.177;}
        else if(P < 19.4){D = 13.1 * sqrt(P - 6.6) + 40;}
        else if(P <= 25){D = 2.17 * P + 44.78;}

        //Catch special cases
        if(D > 99){D = 99;}
        else if(D < 0 || P == 0){D = 0 ;}

        D = D / 100;//blijkbaar werkt het alleen op een aparte regel

        PWM_setDuty(handle, PWM_DUTY_FRACTION_MAX * D);
        return 0;
    }
    return -1; //Init PWM failed
}

/*
 *  ======== mainThread ========
 *  Task periodically increments the PWM duty for the on board LED.
 */
void *mainThread(void *arg0)
{
    int ret = WarmteElement(20);
    if(ret < 0)
    {
        while(1);
    }
}
