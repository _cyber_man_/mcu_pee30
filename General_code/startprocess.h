/*
 *  startprocess.h
 *
 *  Created on: 17 nov. 2019
 *  Author: Edward Bretland (0904269)
 *
 *  Description: This is the header file used by startprocess.c
 *
 *  NOTICE: External dependencies might have to be configured.
 *
 */

#ifndef STARTPROCESS_H_
#define STARTPROCESS_H_

/* Headers */
// C standards:
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
// TI drivers:
#include <ti/drivers/Board.h>
// TI-RTOS:
#include <ti/sysbios/BIOS.h>
// pThreads:
#include <pthread.h>
#include <mqueue.h>
// POSIX:
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>
// UART:
#include "uart_term.h"
// I2C:
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC32XX.h>

/* Defines */
// General:
#define SLEEP_TIME              1

// I2C:
#define SENSORS                 0
#define OPT_ADDR                0x47
#define I2C_CONFIG_INDEX_VAL    0
#define I2C_SLAVE_ADDRESS       0x50
#define I2C_SLAVE_ADR_MOTOR     0x06
#define I2C_SLAVE_ADR_LOAD      0x04
#define I2C_SLAVE_ADR_TEST      0x53
#define I2C_WRITE_COUNT         8
#define I2C_READ_COUNT          8
#define WRITE_BUFFER_SIZE       8
#define READ_BUFFER_SIZE        8
#define I2C_SLEEP               1

/* UNDEF */
#undef COMP_I2C_FUNC
#undef COMP_SPT_FUNC
#undef COMP_SPI_FUNC

/* Data type definitions */
typedef unsigned char   uchar;
typedef unsigned int    uint;
typedef unsigned long   ulong;

/* Enumerators types */
typedef enum
{
    MCU_I2C_ON,
    MCU_I2C_OFF,
    MCU_I2C_EXIT
} enum_MCU_I2C_states;
enum_MCU_I2C_states MCU_I2C_state = MCU_I2C_OFF;

typedef enum
{
    MCU_I2C_RW_ADR_MOTOR,
    MCU_I2C_RW_ADR_LOAD,
    MCU_I2C_RW_ADR_TEST
} enum_MCU_I2C_RW_ADR_states;
enum_MCU_I2C_RW_ADR_states MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_TEST;

typedef enum
{
    MCU_MANUAL,
    MCU_AUTOMATIC
} enum_MA_states;

typedef enum
{
    MCU_booting,
    MCU_booted,
} enum_MCU_boot_states;
enum_MCU_boot_states MCU_boot_states = MCU_booting;

typedef enum
{
    MCU_I2C_READ,
    MCU_I2C_WRITE
} enum_MCU_I2C_RW_states;
enum_MCU_I2C_RW_states MCU_I2C_RW_states = MCU_I2C_READ;

typedef enum
{
    MCU_SP_IDLE,
    MCU_SP_INIT,
    MCU_SP_COMPLETE
} enum_MCU_SP_states;
enum_MCU_SP_states MCU_SPS_states = MCU_SP_IDLE;

#ifdef COMP_SPI_FUNC
typedef enum
{
    MCU_SP_IDLE,
    MCU_SP_INIT,
    MCU_SP_MOTOR,
    MCU_SP_LOAD,
    MCU_SP_COMPLETE,
    MCU_SP_EXIT
} enum_MCU_SP_states;
enum_MCU_SP_states MCU_SP_state = MCU_SP_IDLE;
#endif

/* Structures */
typedef struct
{
    enum_MA_states ManualAutoSelect;
    uint loadRestSetpoint;
    uint loadSetpoint;
    uint SpeedRestSetpoint;
    uint SpeedSetpoint;
} StartProcessParams;

#endif /* STARTPROCESS_H_ */
