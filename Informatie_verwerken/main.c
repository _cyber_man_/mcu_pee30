// C header files
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
// Driver Header files
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC32XX.h>
// RTOS header files
#include <ti/sysbios/BIOS.h>
// POSIX Header files
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>
#include <ti/drivers/dpl/HwiP.h>
#include <ti/drivers/dpl/SemaphoreP.h>
#include <ti/drivers/Power.h>

// Eigen headers
#include "uart_term.h"
#include "bestanden.h"
#include "Board.h"

#define     I2CCC32XX_PIN_01_I2C_SCL   0x100 //PIN_1 voor I2C_SCL
#define     I2CCC32XX_PIN_02_I2C_SDA   0x101 //PIN_2 voor I2C_SDA

/**
 * main.c
 */

int main(void)
{

    typedef struct{
        char B[4]; //Max spanning
        char I[4]; //Stroom DUT
        char J[4]; //Max stroom
        char N[4]; //Rendement aandrijving
        char S[4]; //Snelheid setpoint
        char U[4]; //Spanning DUT
        char V[4]; //Snelheid
    }aandrijving;

    bool status;
    I2C_init();
    // initialize optional I2C bus parameters
    I2C_Params params;
    I2C_Params_init(&params);
    params.bitRate = I2C_400kHz;
    // Open I2C bus for usage
    I2C_Handle i2cHandle = I2C_open(0, &params);
    // Initialize slave address of transaction

    strcpy();
    I2C_Transaction i2cTransaction = {0};
    uint8_t readBuffer[4];
    uint8_t writeBuffer[6];
    char B[4]; //Max spanning
    char I[4]; //Stroom DUT
    char J[4]; //Max stroom
    char N[4]; //Rendement aandrijving
    char S[4]; //Snelheid setpoint
    char U[4]; //Spanning DUT
    char V[4]; //Snelheid
    writeBuffer[0] = 0xAB;
    writeBuffer[1] = 0xCD;
    i2cTransaction.slaveAddress = 0x4; // adres aandrijving
    //i2cTransaction.slaveAddress = 0x6; // adres belasting
    i2cTransaction.writeBuf = I;
    i2cTransaction.writeBuf = N;
    i2cTransaction.writeBuf = U;
    i2cTransaction.writeBuf = V;
    i2cTransaction.writeBuf = B;
    i2cTransaction.writeBuf = J;
    i2cTransaction.writeBuf = S;
    i2cTransaction.writeCount = sizeof(I);
    i2cTransaction.writeCount = sizeof(N);
    i2cTransaction.writeCount = sizeof(U);
    i2cTransaction.writeCount = sizeof(V);
    i2cTransaction.writeCount = sizeof(B);
    i2cTransaction.writeCount = sizeof(J);
    i2cTransaction.writeCount = sizeof(S);

    i2cTransaction.readCount = 7;
    status = I2C_transfer(i2cHandle, &i2cTransaction);
    if (status == false) {
        printf("Fout bij I2C transfer.\n");
        // Unsuccessful I2C transfer
    }

    //I2C_close(i2cHandle);

//inputs:
//char I[4]; //Stroom DUT
//char N[4]; //Rendement aandrijving
//char M[4]; //rendement belasting
//char U[4]; //Spanning DUT
//char K[4]; //Koppel belasting motor
//char Q[4]; //Vermogen belasting motor
//char V[4]; //Snelheid
//char A[4]; //Temperatuur
//
//outputs:
//char B[4]; //Max spanning
//char C[4]; //Max snelheid
//char T[4]; //Max temperatuur
//char J[4]; //Max stroom
//char S[4]; //Snelheid setpoint
//char W[4]; //Vermogen setpoint
//char P[4]; //Max vermogen

geen aandrijving en belasting?
//char K[4]; //Koppel belasting motor

aandrijving:
char B[4]; //Max spanning
char I[4]; //Stroom DUT
char J[4]; //Max stroom
char N[4]; //Rendement aandrijving
char S[4]; //Snelheid setpoint
char U[4]; //Spanning DUT
char V[4]; //Snelheid

belasting:
char A[4]; //Temperatuur
char C[4]; //Max snelheid
char M[4]; //rendement belasting
char P[4];
char Q[4];
char T[4];
char W[4];
	return 0;
}
