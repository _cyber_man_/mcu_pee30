/*
 *  startprocess.c
 *
 *  Created on: 17 nov. 2019
 *  Author: Edward Bretland (0904269)
 *
 *  Description: This source file contains the start process (BEHEER STARTPROCES)
 *  functionalities. After booting the MCU, the start process will run and manage
 *  the initialization parameters (manual/automatic, set-points for speed and load).
 *
 *  NOTICE: External dependencies might have to be configured.
 *
 *  TODO: Properly setup post box send and receive functionality.
 *
 *  Sources: sscanf & sprintf
*            http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/tirtos/2_20_00_06/exports/tirtos_full_2_20_00_06/products/tidrivers_full_2_20_00_08/docs/doxygen/html/_i2_c_8h.html
*            http://dev.ti.com/tirex/content/simplelink_cc32xx_sdk_3_20_00_06/docs/tidrivers/doxygen/html/_i2_c_c_c32_x_x_8h.html
 *           http://dev.ti.com/tirex/content/simplelink_cc32xx_sdk_3_20_00_06/docs/tidrivers/doxygen/html/_i2_c_8h.html#ac5d827b67fe77d7d179026941cc069d7
 *
 */

/*****************************************************************************

    Initialization

*****************************************************************************/

/* Headers */
#include <startprocess.h>
#include <stopprocess.h>
#include <external_globals.h>

/* Globals */
char g_InitParameters[G_BUFFER_SIZE_EXT];

/* Function definitions */
void InitStartProcess();                            // Initialization function
mqd_t InitStartProcessPostbox();                    // Initialization Post-box
void InitStartProcessThread();                      // Initialization pThread
void InitStartProcessParamThread();                 // Initialization pThread
void InitStopProcessThread();                       // Initialization pThread

void *StartProcessThread();                         // pThread
void *StartProcessParamThread();                    // pThread
void *StopProcessThread();                          // pThread

void StopProcessCallback(uint_least8_t index);      // Callback function



/*****************************************************************************

    Functions

*****************************************************************************/

/* Initialization functions */
void InitStartProcess(mqd_t Parameters)
{
    /* Start-up of the StartProcess functionalities */
    mqd_t InitStartProcessPostbox();
    InitStartProcessThread(Parameters);
    InitStopProcessThread(Parameters);
}

mqd_t InitStartProcessPostbox()
{
    /* Setup Post-box for Start & Stop Processes */
    mqd_t StartProcessPostbox;

    /* m-queue attributes */
    mq_attr mq_attr_StartProcessPostbox;
    mq_attr_StartProcessPostbox.mq_flags = 0;
    mq_attr_StartProcessPostbox.mq_maxmsg = 4;
    mq_attr_StartProcessPostbox.mq_msgsize = 32;

    mq_open("StartProcessPostbox", (O_RDWR | O_CREAT), 0666, &mq_attr_StartProcessPostbox);
    StartProcessPostbox = mq_open("StartProcessPostbox", (O_RDWR | O_CREAT), 0666, &mq_attr_StartProcessPostbox);

    UART_PRINT("\n\r Successfully initialized StartProcessPostbox! \n\r");
    return StartProcessPostbox;
}
void InitStartProcessThread(mqd_t StartProcessPostbox)
{
    /* Create pThread for StartProcess */

    pthread_attr_t pAttrs;
    pthread_attr_init(&pAttrs);
    struct sched_param priParam;
    priParam.sched_priority = 8;
    int retc = pthread_attr_setdetachstate(&pAttrs, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setschedparam(&pAttrs, &priParam);
    retc |= pthread_attr_setstacksize(&pAttrs, 4096);
    pthread_t thread;
    retc |= pthread_create(&thread, &pAttrs, StartProcessThread, &StartProcessPostbox);

    /* Return error check */
    if(retc != 0)
    {
        UART_PRINT("\n\r Failed to create StartProcessThread! \n\r");
        while(1);
    }
}
void InitStartProcessParamThread(mqd_t StartProcessPostbox)
{
    /* Create pThread for StartProcessParamThread */

    pthread_attr_t pAttrs;
    pthread_attr_init(&pAttrs);
    struct sched_param priParam;
    priParam.sched_priority = 8;
    int retc = pthread_attr_setdetachstate(&pAttrs, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setschedparam(&pAttrs, &priParam);
    retc |= pthread_attr_setstacksize(&pAttrs, 4096);
    pthread_t thread;
    retc |= pthread_create(&thread, &pAttrs, StartProcessParamThread, &StartProcessPostbox);

    /* Return error check */
    if(retc != 0)
    {
        UART_PRINT("\n\r Failed to create StartProcessParamThread! \n\r");
        while(1);
    }
}
void InitStopProcessThread(mqd_t StartProcessPostbox)
{
    /* Create pThread for Stop(Process) */

    pthread_attr_t pAttrs;
    pthread_attr_init(&pAttrs);
    struct sched_param priParam;
    priParam.sched_priority = 8;
    int retc = pthread_attr_setdetachstate(&pAttrs, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setschedparam(&pAttrs, &priParam);
    retc |= pthread_attr_setstacksize(&pAttrs, 4096);
    pthread_t thread;
    retc |= pthread_create(&thread, &pAttrs, StopProcessThread, &StartProcessPostbox);

    /* Return error check */
    if(retc != 0)
    {
        UART_PRINT("\n\r Failed to create StopProcessThread! \n\r");
        while(1);
    }
}



/*****************************************************************************

    I2C read & write

*****************************************************************************/
#ifdef COMP_I2C_FUNC
uint8_t I2C_READ(uint i2cSlaveAddress)
{
    /* Read the I2C line */

    /* Types */
    I2C_Handle      i2cHandle;
    I2C_Params      params;
    I2C_Transaction i2cTransaction = {0};

    /* Buffer */
    char readBuffer[READ_BUFFER_SIZE] = {0};

    /* I2C read parameters */
    i2cTransaction.slaveAddress = i2cSlaveAddress;
    i2cTransaction.writeBuf = NULL;
    i2cTransaction.writeCount = 0;
    i2cTransaction.readBuf = readBuffer;
    i2cTransaction.readCount = I2C_READ_COUNT;

    /* I2C read transfer */
    uint8_t status = I2C_transfer(i2cHandle, &i2cTransaction);
    if(status == false){UART_PRINT("\r\n (I2C_READ): Unsuccessful I2C read transfer! \r\n");}
    if(status == true){UART_PRINT("\r\n (I2C_READ): Successful I2C read transfer! \r\n");}
    return readBuffer[READ_BUFFER_SIZE];
}
uint8_t I2C_WRITE(uint i2cSlaveAddress,char writeBuffer[WRITE_BUFFER_SIZE])
{
    /* Write to I2C line */

    /* Types */
    I2C_Handle      i2cHandle;
    I2C_Params      params;
    I2C_Transaction i2cTransaction = {0};

    /* I2C write parameters */
    i2cTransaction.slaveAddress = i2cSlaveAddress;
    i2cTransaction.writeBuf = writeBuffer;
    i2cTransaction.writeCount = I2C_WRITE_COUNT;
    i2cTransaction.readBuf = NULL;
    i2cTransaction.readCount = 0;

    /* I2C write transfer */
    uint8_t status = I2C_transfer(i2cHandle, &i2cTransaction);
    if(status == false){UART_PRINT("\r\n (I2C_WRITE): Unsuccessful I2C write transfer! \r\n");}
    if(status == true){UART_PRINT("\r\n (I2C_WRITE): Successful I2C write transfer! \r\n");}
    return status;
}
#endif



/*****************************************************************************

    Start Process

*****************************************************************************/

/* pThreads */
void *StartProcessThread(void *pvParameters)
{
    /* START PROCESS THREAD */

    /* Start up I2C */
    I2C_init();                             // Initialize I2C
    MCU_I2C_state = MCU_I2C_ON;             // Turn on I2C while loop
    MCU_SPS_states = MCU_SP_INIT;            // Initialize Start Process

    UART_PRINT("\r\n StartProcessThread has been started! \r\n");

    /* Read & write buffers for I2C */
    char readBuffer[READ_BUFFER_SIZE]   = {0};
    char writeBuffer[WRITE_BUFFER_SIZE] = {0};

    /* Placeholder values for buffering */
    char            motorReadData[READ_BUFFER_SIZE]  =  {0};
    char            loadReadData[READ_BUFFER_SIZE]   =  {0};

    /* I2C initialization */
    uint            i = 0;
    I2C_Handle      i2cHandle;
    I2C_Params      params;
    I2C_Transaction i2cTransaction = {0};
    uint            i2cSlaveAddress;
    I2C_Params_init(&params);
    params.transferMode  = I2C_MODE_BLOCKING;
    params.bitRate = I2C_100kHz;

    /* Open I2C */
    i2cHandle = I2C_open(I2C_CONFIG_INDEX_VAL, &params);
    if(i2cHandle == NULL){UART_PRINT("\r\n Error opening I2C! \r\n");}
    UART_PRINT("\r\n I2C has been opened! \r\n");

    /* I2C idle state */
    while(MCU_I2C_state == MCU_I2C_OFF){sleep(I2C_SLEEP);}

    /* Main I2C functionality */
    while(MCU_I2C_state == MCU_I2C_ON)
    {
        if(MCU_SPS_states = MCU_SP_INIT)
        {
            /* Start up initialization */
            i = 1;

        }
        if(i == 1)
        {
            /* Write data to MOTOR */
            MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_MOTOR;
            MCU_I2C_RW_states = MCU_I2C_WRITE;
            writeBuffer[0] = 'a';
            writeBuffer[1] = 'b';
            writeBuffer[8] = 'c';
            // -> DATA TO MOTOR

        }
        if(i == 2)
        {
            /* Write data to LOAD */
            MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_LOAD;
            MCU_I2C_RW_states = MCU_I2C_WRITE;
            writeBuffer[0] = 'd';
            writeBuffer[1] = 'e';
            writeBuffer[8] = 'f';
            // -> DATA TO LOAD

        }
        if(i == 3)
        {
            /* Read from MOTOR */
            MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_MOTOR;
            MCU_I2C_RW_states = MCU_I2C_READ;
            // <- DATA FROM MOTOR

        }
        if(i == 4)
        {
            /* Read from LOAD */
            MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_LOAD;
            MCU_I2C_RW_states = MCU_I2C_READ;
            // < DATA FROM LOAD

        }
        if(i == 5)
        {
            /* Initialization completed */
            MCU_I2C_RW_states = MCU_I2C_READ;
            MCU_SPS_states = MCU_SP_COMPLETE;
            i = 6;
        }

        /* I2C address states */
        if(MCU_I2C_RW_ADR_state == MCU_I2C_RW_ADR_MOTOR)
        {
            /* Motor address */
            i2cSlaveAddress = I2C_SLAVE_ADR_MOTOR;
            UART_PRINT("\r\n I2C addressing motor. \r\n");
        }
        if(MCU_I2C_RW_ADR_state == MCU_I2C_RW_ADR_LOAD)
        {
            /* Load address */
            i2cSlaveAddress = I2C_SLAVE_ADR_LOAD;
            UART_PRINT("\r\n I2C addressing load. \r\n");
        }
        if(MCU_I2C_RW_ADR_state == MCU_I2C_RW_ADR_TEST)
        {
            /* Test address */
            i2cSlaveAddress = I2C_SLAVE_ADR_TEST;
            UART_PRINT("\r\n I2C addressing test. \r\n");
        }

        /* I2C transaction (READ) */
        if(MCU_I2C_RW_states == MCU_I2C_READ)
        {
            /* I2C read parameters */
            i2cTransaction.slaveAddress = i2cSlaveAddress;
            i2cTransaction.writeBuf = NULL;
            i2cTransaction.writeCount = 0;
            i2cTransaction.readBuf = readBuffer;
            i2cTransaction.readCount = I2C_READ_COUNT;

            /* I2C read transfer */
            uint8_t status = I2C_transfer(i2cHandle, &i2cTransaction);
            if(status == false){UART_PRINT("\r\n Unsuccessful I2C read transfer! \r\n");}
            if(status == true){UART_PRINT("\r\n Successful I2C read transfer! \r\n");}
            if((i > 0)&(i < 6)){i++;}
            sleep(I2C_SLEEP);
        }

        /* I2C transaction (WRITE) */
        if(MCU_I2C_RW_states == MCU_I2C_WRITE)
        {
            /* I2C write parameters */
            i2cTransaction.slaveAddress = i2cSlaveAddress;
            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = strlen(writeBuffer);
            i2cTransaction.readBuf = NULL;
            i2cTransaction.readCount = 0;

            /* I2C write transfer */
            uint8_t status = I2C_transfer(i2cHandle, &i2cTransaction);
            if(status == false){UART_PRINT("\r\n Unsuccessful I2C write transfer! \r\n");}
            if(status == true){UART_PRINT("\r\n Successful I2C write transfer! \r\n");}
            if((i > 0)&(i < 6)){i++;}
            sleep(I2C_SLEEP);
        }
        else{sleep(I2C_SLEEP);}
    }

    /* Idle for I2C */
    while(MCU_I2C_state == MCU_I2C_OFF){sleep(I2C_SLEEP);}

    /* Exit for I2C */
    if(MCU_I2C_state == MCU_I2C_EXIT)
    {I2C_close(i2cHandle); UART_PRINT("\r\n I2C has been closed! \r\n");}
    return NULL;
}


#ifdef COMP_SPI_FUNC
void *StartProcessThread(void *pvParameters)
{
    /* START PROCESS THREAD */

    /* Start up I2C */
    I2C_init();
    MCU_I2C_state = MCU_I2C_ON;
    UART_PRINT("\r\n StartProcessThread has been started! \r\n");

    /* Read & write buffers for I2C */
    char readBuffer[READ_BUFFER_SIZE]   = {0};
    char writeBuffer[WRITE_BUFFER_SIZE] = {0};

    /* I2C initialization */
    I2C_Handle      i2cHandle;
    I2C_Params      params;
    I2C_Transaction i2cTransaction = {0};
    uint            i2cSlaveAddress;
    I2C_Params_init(&params);
    params.transferMode  = I2C_MODE_BLOCKING;
    params.bitRate = I2C_100kHz;

    /* Open I2C */
    i2cHandle = I2C_open(I2C_CONFIG_INDEX_VAL, &params);
    if(i2cHandle == NULL){UART_PRINT("\r\n Error opening I2C! \r\n");}
    UART_PRINT("\r\n I2C has been opened! \r\n");

    /* I2C idle state */
    while(MCU_I2C_state == MCU_I2C_OFF){sleep(I2C_SLEEP);}

    /* Test values for buffer */
    //writeBuffer[READ_BUFFER_SIZE]  = "0101";
    //readBuffer[WRITE_BUFFER_SIZE]  = "1010";

    /* Test commands */
    //MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_LOAD;
    //MCU_I2C_RW_states = MCU_I2C_WRITE;
    //writeBuffer[WRITE_BUFFER_SIZE] = "abcd";

    /* Main I2C functionality */
    while(MCU_I2C_state == MCU_I2C_ON)
    {
        /* I2C address states */
        if(MCU_I2C_RW_ADR_state == MCU_I2C_RW_ADR_MOTOR)
        {
            /* Motor address */
            i2cSlaveAddress = I2C_SLAVE_ADR_MOTOR;
            UART_PRINT("\r\n I2C addressing motor. \r\n");
        }
        if(MCU_I2C_RW_ADR_state == MCU_I2C_RW_ADR_LOAD)
        {
            /* Load address */
            i2cSlaveAddress = I2C_SLAVE_ADR_LOAD;
            UART_PRINT("\r\n I2C addressing load. \r\n");
        }
        if(MCU_I2C_RW_ADR_state == MCU_I2C_RW_ADR_TEST)
        {
            /* Test address */
            i2cSlaveAddress = I2C_SLAVE_ADR_TEST;
            UART_PRINT("\r\n I2C addressing test. \r\n");
        }

        /* I2C transaction (READ) */
        if(MCU_I2C_RW_states == MCU_I2C_READ)
        {
            /* I2C read parameters */
            i2cTransaction.slaveAddress = i2cSlaveAddress;
            i2cTransaction.writeBuf = NULL;
            i2cTransaction.writeCount = 0;
            i2cTransaction.readBuf = readBuffer;
            i2cTransaction.readCount = I2C_READ_COUNT;

            /* I2C read transfer */
            uint8_t status = I2C_transfer(i2cHandle, &i2cTransaction);
            if(status == false){UART_PRINT("\r\n Unsuccessful I2C read transfer! \r\n");}
            if(status == true){UART_PRINT("\r\n Successful I2C read transfer! \r\n");}
            sleep(I2C_SLEEP);
        }

        /* I2C transaction (WRITE) */
        if(MCU_I2C_RW_states == MCU_I2C_WRITE)
        {
            /* I2C write parameters */
            i2cTransaction.slaveAddress = i2cSlaveAddress;
            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = strlen(writeBuffer);
            i2cTransaction.readBuf = NULL;
            i2cTransaction.readCount = 0;

            /* I2C write transfer */
            uint8_t status = I2C_transfer(i2cHandle, &i2cTransaction);
            if(status == false){UART_PRINT("\r\n Unsuccessful I2C write transfer! \r\n");}
            if(status == true){UART_PRINT("\r\n Successful I2C write transfer! \r\n");}
            MCU_I2C_RW_states = MCU_I2C_READ;
        }
        else{sleep(I2C_SLEEP);}
    }

    /* Idle for I2C */
    while(MCU_I2C_state == MCU_I2C_OFF){sleep(I2C_SLEEP);}

    /* Exit for I2C */
    if(MCU_I2C_state == MCU_I2C_EXIT)
    {I2C_close(i2cHandle); UART_PRINT("\r\n I2C has been closed! \r\n");}
    return NULL;
}
#endif

void *StartProcessParamThread(void *pvParameters)
{
    /* STAR PROCESS PARAM THREAD */

    /* Parameters */
    StartProcessParams      ProcessParams;

    /* Initialization */
    ProcessParams.ManualAutoSelect = MCU_AUTOMATIC;
    ProcessParams.SpeedRestSetpoint = 0;
    ProcessParams.SpeedSetpoint = 0;
    ProcessParams.loadRestSetpoint = 0;
    ProcessParams.loadSetpoint = 0;

    /* Idle */
    //while(MCU_SP_state == MCU_SP_IDLE){sleep(SLEEP_TIME);}
    //UART_PRINT("\r\n StartProcessParamThread running ...\r\n");

    #ifdef COMP_SPT_FUNC

    /* Main functionality */
    while(MCU_SP_state == MCU_SP_INIT)
    {



    }



    /* Main functionality */
    while(1)
    {
        switch(MCU_SP_state)
        {
            case MCU_SP_IDLE:
            {
                sleep(SLEEP_TIME);
                break;
            }

            case MCU_SP_INIT:
            {
                /* Load data */



                /* Move forward */
                MCU_SP_state = MCU_SP_MOTOR;
                break;
            }

            case MCU_SP_MOTOR:
            {
                /* Load data */
                MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_MOTOR;
                MCU_I2C_RW_states = MCU_I2C_WRITE;

                /*
                 *  Send the following data to MOTOR:
                 *      - Start (?)
                 *      - Start initialization (?)
                 *      - Speed rest set-point
                 *      - Speed set-point
                 *      - Max. current
                 *      - Max. voltage
                 *
                 *  Return from MOTOR:
                 *      - MOTOR is ready
                 */

                /* Move forward */
                MCU_SP_state = MCU_SP_LOAD;
                break;
            }

            case MCU_SP_LOAD:
            {
                /* Load data */
                MCU_I2C_RW_ADR_state = MCU_I2C_RW_ADR_LOAD;
                MCU_I2C_RW_states = MCU_I2C_WRITE;

                /*
                 *  Send the following data to LOAD:
                 *      - Start (?)
                 *      - Start initialization (?)
                 *      - Load rest set-point
                 *      - Load set-point
                 *      - Max. temperature
                 *      - Max. Speed
                 *      - Max. power
                 *
                 *  Return from LOAD:
                 *      - LOAD is ready
                 */

                /* Move forward */
                MCU_SP_state = MCU_SP_COMPLETE;
                break;
            }

            case MCU_SP_COMPLETE:
            {
                /* Load data */

                /*
                 *  Message system that initialization procedure
                 *  has been completed.
                 *
                 */

                /* Move forward */
                MCU_SP_state = MCU_SP_EXIT;
                break;
            }

            case MCU_SP_EXIT:
            {
               /* Exit */
               sleep(SLEEP_TIME);
               return NULL;
            }
        }

    }

    #endif

    /* Finished */
    return NULL;
}



/*****************************************************************************

    Stop Process

*****************************************************************************/

void *StopProcessThread(void *pvParameters)
{
    /* STOP (PROCESS) THREAD */

    /* Initialization */
    GPIO_init();

    /* Configuration of GPIO */
    GPIO_setConfig(MCU_GPIO_STOP_BUTTON, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);

    /* Install button callback */
    GPIO_setCallback(MCU_GPIO_STOP_BUTTON, StopProcessCallback);

    /* Enable interrupts */
    GPIO_enableInt(MCU_GPIO_STOP_BUTTON);
    UART_PRINT("\r\n StopProcess pThread successfully initialized! \r\n");


    while(MCU_operational_states != MCU_safety_mode){sleep(SLEEP_TIME);}
    return NULL;
}
void StopProcessCallback(uint_least8_t index)
{
    /* Callback function for StopProcess (ISR) */
    MCU_operational_states = MCU_safety_mode;
    int i;
    UART_PRINT("\r\n StopProcess callback ISR triggered! [%d]", i++ );
}

/* End of startprocess.c code */
