// C header files
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
// Driver Header files
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>
// RTOS header files
#include <ti/sysbios/BIOS.h>
// POSIX Header files
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>

// Eigen headers
#include "uart_term.h"
#include "bestanden.h"

// Taken
void *App_Task(void *args);

// Functies
void PingResultaat(SlNetAppPingReport_t *pReport);
int StelWifiIn(SlWlanSecParams_t *SecParams, SlWlanSecParamsExt_t *SecExtParams, _i8 *wachtwoord, bool vraagOmWachtwoord);
void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args));

// Semaphore voor taaksynchronisatie
sem_t wachtenOpIP;

// Wifi instellingen
#define IDENTITY ""
//#define SSID "AndroidLP"
//#define netwerkww "tiesiepie ajpie"
#define SSID "taart"
#define netwerkww "abcdefgh"
//#define SSID "AndroidDV"
//#define netwerkww "Enkjelftw"
//#define SSID "eduroam"
//#define netwerkww ""

bool startInitButton = false;
bool draaiknop = false;


/*---Callbacks---*/
void startInitialisatie(uint_least8_t index){
    startInitButton = true;
}

void rotary(uint_least8_t index){
    draaiknop = true;
}

void *mainThread(void *arg0)
{
    // Variabelen
    int Status;
    SlWlanSecParams_t SecParams; // Wifi parameters
    SlWlanSecParamsExt_t SecExtParams; // Wifi enterprise parameters
    _i8 ww[64]; // Buffer om een wifi-wachtwoord in op te slaan

    // Intialisaties
    sem_init(&wachtenOpIP, 0, 0); // Semaphore voor synchronisatie
    SPI_init();
    GPIO_init();
    InitTerm(); // UART functionaliteit van TI

    // Prio stack functie
    MaakTaak(9, 2048, sl_Task); // Nodig voor wifi api
    MaakTaak(2, 2048, App_Task); // Onze aplicatie

    /*----GPIO----*/
    GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUTPUT);

    // Hello
    printf("\nApplicatie gestart.\nOverige meldingen zijn te vinden op de UART terminal op 115200 baud.\n");
    UART_PRINT("Starten.\r\n");

    bool vraagOmWachtwoord = true; // Probeer eerst om opgeslagen wachtwoord te gebruiken
    do
    {
        // Wifi instellen en verbinden
        StelWifiIn(&SecParams, &SecExtParams, netwerkww, false);

        Status = sl_WlanConnect((_i8*)SSID, // SSID
                                strlen(SSID), // Lengte van SSID
                                0,
                                &SecParams, // Security parameters wifi
                                0); // Extended security parameters enterprice wifi
//        &SecExtParams
        if (Status !=0)
        {
            UART_PRINT("Wifi instellingen fout: %d\r\n", Status);
            vraagOmWachtwoord = true; // Probeer nogmaals om een correct wachtwoord in te voeren
        }
    }
    while (Status != 0);

    /*!
     * De thread kan nu stoppen.
     * Wachten op succesvolle verbinding zodat de App-thread
     * verder kan gaan.
     *
     * @see: SimpleLinkNetAppEventHandler(),
     * @see: App_Task(void * args)
     *
     */
    return 0;
}


void *App_Task(void *args)
{
    _u32 EnkjelIP = SL_IPV4_VAL(192,168,43,57);
    _u32 Ivar = SL_IPV4_VAL(192,168,43,179);
    _u32 AndroidLPIP = SL_IPV4_VAL(192,168,43,179);
    _u16 len = sizeof(SlNetCfgIpV4Args_t);
    _u16 ConfigOpt = 0;
    _u32 googleIP = EnkjelIP;
    SlNetCfgIpV4Args_t ipV4 = {0}; // Struct voor ip
    SlNetAppPingReport_t report; // Struct voor ping
    SlNetAppPingCommand_t pingCommand; // Struct voor ping

    /* Wachten op een ip adres, daarna kunnen we verder.
     * Zie ook SimpleLinkNetAppEventHandler() */
    sem_wait(&wachtenOpIP);

    // Wachtwoord alleen opslaan als verbinden gelukt is.
    schrijfWachtwoord();

    // Haal IP op
    sl_NetCfgGet(SL_NETCFG_IPV4_STA_ADDR_MODE,
                 &ConfigOpt,
                 &len,
                 (_u8 *)&ipV4
                );

    // Pretty print IP
    UART_PRINT( "\r\n"
                "Mijn IP\t %d.%d.%d.%d\r\n"
                "MASK\t %d.%d.%d.%d\r\n"
                "GW\t %d.%d.%d.%d\r\n"
                "DNS\t %d.%d.%d.%d\r\n",
                SL_IPV4_BYTE(ipV4.Ip,3), SL_IPV4_BYTE(ipV4.Ip,2), SL_IPV4_BYTE(ipV4.Ip,1), SL_IPV4_BYTE(ipV4.Ip,0),
                SL_IPV4_BYTE(ipV4.IpMask,3), SL_IPV4_BYTE(ipV4.IpMask,2), SL_IPV4_BYTE(ipV4.IpMask,1), SL_IPV4_BYTE(ipV4.IpMask,0),
                SL_IPV4_BYTE(ipV4.IpGateway,3), SL_IPV4_BYTE(ipV4.IpGateway,2), SL_IPV4_BYTE(ipV4.IpGateway,1), SL_IPV4_BYTE(ipV4.IpGateway,0),
                SL_IPV4_BYTE(ipV4.IpDnsServer,3), SL_IPV4_BYTE(ipV4.IpDnsServer,2), SL_IPV4_BYTE(ipV4.IpDnsServer,1), SL_IPV4_BYTE(ipV4.IpDnsServer,0));

    // DNS request naar google.nl, wat is het IP?
//    sl_NetAppDnsGetHostByName("www.google.nl", strlen("www.google.nl"), &googleIP, SL_AF_INET);

    //Wat willen we pingen?
    pingCommand.Ip = googleIP;
    pingCommand.PingSize = 32; // Size of ping, in bytes
    pingCommand.PingIntervalTime = 100; // Delay between pings, in milliseconds
    pingCommand.PingRequestTimeout = 1000; // Timeout for every ping in milliseconds
    pingCommand.TotalNumberOfAttempts = 4; // Max number of ping requests. 0 - forever
    pingCommand.Flags = 0; // Report only when finished

    // Feedback dat we starten met de ping operatie
    UART_PRINT("\r\nping naar mij (%d.%d.%d.%d)\r\n",
               SL_IPV4_BYTE(googleIP,3),
               SL_IPV4_BYTE(googleIP,2),
               SL_IPV4_BYTE(googleIP,1),
               SL_IPV4_BYTE(googleIP,0));


    // Ping uitvoeren
//    sl_NetAppPing(&pingCommand, SL_AF_INET, &report, PingResultaat);

    /*----CLIENT----*/
    _i16 handle;

    handle = sl_Socket(SL_AF_INET, SL_SOCK_STREAM, SL_IPPROTO_TCP);
    if(handle < 0){
        UART_PRINT("[SETUP][ERROR]Failed to setup socket:\t%d\r\n", handle);
        while(1);
    }
    UART_PRINT("Handle =\t%d\r\n", handle);

    SlSockAddrIn_t socketAddress;
    socketAddress.sin_family = SL_AF_INET;
    socketAddress.sin_port = sl_Htons(2000);
    socketAddress.sin_addr.s_addr = sl_Htonl(googleIP);

    handle = sl_Connect(handle, &socketAddress, sizeof(socketAddress));
    if(handle < 0){
        UART_PRINT("[SETUP][ERROR]Failed to setup connection:\t%d\r\n", handle);
        UART_PRINT("Right IP-address?\r\n");
        while(1);
    }
//    UART_PRINT("Wait 1 second...\r\n");
//    sleep(1);
    UART_PRINT("alright...Time to send stuff :D\r\n*************\r\n");

    _i16 ret;
    int delayRotary = 0;
    int MSG_LEN = 20;
    char* message = "3";
//    char msg[6] = {"1","32","14","54","37","112"};
    char msg[8] = {13,2,0,1,1,1,73,4};
    char recv1[1] = {NULL};
    char recv2[140] = {NULL};

    int i;
    int waitTime = 1;

//    while(startInitButton == false){
//        if(draaiknop == true){
//            /*sending*/
//            UART_PRINT("current message (type d):\t%d\r\n", msg);
//            UART_PRINT("current address (type d):\t%d\r\n", &msg);
//            ret = sl_Send(handle, &msg, 8, NULL);
//            if(ret < 0){
//                UART_PRINT("[SOCKET][ERROR]Message wasn't send:\t%d\r\n", ret);
//                while(1);
//            }
//            GPIO_toggle(Board_GPIO_LED2);
//            UART_PRINT("[SOCKET]Message was send(type s):\t'%s'\r\n", msg);
//            draaiknop = false;
//        }
//    }

    int i = 1200;
    while(i != 1170){

        ret = sl_Send(handle, &i, 1, NULL);
        i--;
        delay(1000);

    }
    /*sending*/
    msg[0] = 'S';
    msg[1] = 'I';
    UART_PRINT("current message (type d):\t%d\r\n", msg);
    UART_PRINT("current address (type d):\t%d\r\n", &msg);
    ret = sl_Send(handle, &msg, 8, NULL);
    if(ret < 0){
        UART_PRINT("[SOCKET][ERROR]Message wasn't send:\t%d\r\n", ret);
        while(1);
    }
    GPIO_toggle(Board_GPIO_LED2);
    UART_PRINT("[SOCKET]Message was send(type s):\t'%s'\r\n", msg);
    draaiknop = false;
    int parameters = 0;
    int compleet = 0;
    int ontvangenbyte =0;
    while(compleet==0){

        /*receiving*/
        ret = sl_Recv(handle, &recv1, 7, NULL);
        if(ret < 0){
            UART_PRINT("[SOCKET][ERROR]Message wasn't received:\t%d\r\n", ret);
            while(1);
        }
        int iets = 0;
        if (recv1[0] == 109){
            parameters = 38;

        }
        //else if (asciiToInt(0, recv1[0]) == "p"  ){
        else if (recv1[0] == 112){
            parameters = 140;
        }

//        while(iets < parameters){
//            ret = sl_Recv(handle, &recv2[iets], 7, NULL);
//            iets++;
//            if(ret < 0){
//                UART_PRINT("[SOCKET][ERROR]Message wasn't received:\t%d\r\n", ret);
//                while(1);
//            }
//        }
        ret = sl_Recv(handle, &recv2[iets], parameters, NULL);
        if(ret < 0){

            UART_PRINT("[SOCKET][ERROR]Message wasn't received:\t%d\r\n", ret);
            while(1);
        }

        GPIO_toggle(Board_GPIO_LED0);
//        UART_PRINT("[SOCKET]Message was received(type d):\t'%d'\r\n", recv);
        UART_PRINT("[SOCKET]Message was received(type s):\t'%s'\r\n", recv2);
        UART_PRINT("Newly formed message:\t");
        for(i = 0 ; i < strlen(recv2) ; i++){
            recv2[i] = asciiToInt(i, recv2[i]);
            UART_PRINT("%d", recv2[i]);
        }
        UART_PRINT("\r\n");
        if(recv1[0] == 'H'){
            UART_PRINT("[MISC]'H' has been received\r\n");
            GPIO_toggle(Board_GPIO_LED1);
        }
    }

        while(1){
        /*sending*/
        UART_PRINT("current message (type d):\t%d\r\n", msg);
        UART_PRINT("current address (type d):\t%d\r\n", &msg);
        ret = sl_Send(handle, &msg, 8, NULL);
        if(ret < 0){
            UART_PRINT("[SOCKET][ERROR]Message wasn't send:\t%d\r\n", ret);
            while(1);
        }
        GPIO_toggle(Board_GPIO_LED2);
        UART_PRINT("[SOCKET]Message was send(type s):\t'%s'\r\n", msg);

//        /*pausing*/
        UART_PRINT("waiting %d seconds...\r\n", waitTime);
        sleep(waitTime);
    }


    // Deze thread is klaar. Nu wachten op PingResultaat.
    return NULL;
}

int asciiToInt(int i, char waarde){
    if(waarde != NULL){
        waarde-= 48;
        return(waarde);
    }
}

// Callback van ping functie
void PingResultaat(SlNetAppPingReport_t* pReport)
{
    // Resultaten afdrukken.
    UART_PRINT("Verzonden: %d, "
               "Ontvangen: %d\r\n"
               "Gemiddelde Responsietijd: %dms\r\n",
               pReport->PacketsSent,
               pReport->PacketsReceived,
               pReport->AvgRoundTime);

    // Klaar met ping applicatie
}

void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args))
{
    pthread_t Thread;
    pthread_attr_t attrs;
    struct sched_param priParam;
    int retc;

    // sl_task thread maken. Dit handelt alle callbacks van wifi af
    priParam.sched_priority = prioriteit;
    retc = pthread_attr_init(&attrs);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED); // Ontkoppelde taak
    retc |= pthread_attr_setschedparam(&attrs, &priParam); // Prioriteit instellen
    retc |= pthread_attr_setstacksize(&attrs, stackSize); // Stacksize instellen

    retc |= pthread_create(&Thread, &attrs, functie, NULL); // Taak starten
    if (retc != 0)
    {
        // pthread_create() failed
        UART_PRINT("Taak fout: %d",retc);
        while (1);
    }
}

int StelWifiIn(SlWlanSecParams_t* SecParams, SlWlanSecParamsExt_t* SecExtParams, _i8* wachtwoord, bool vraagOmWachtwoord)
{
    int Status;
    SlDateTime_t dateTime = {0};
    _u8 serverAuthenticatie = 0;

    // NWP starten en configureren
    sl_Start(0, 0, 0);
    Status = sl_WlanSetMode(ROLE_STA); // Station modus

    /* Om te voorkomen dat jouw wachtwoord in je code is te vinden wordt het
     * geschreven naar een bestand op de flash.
     *
     * Je moet eenmalig het wachtwoord geven, elke keer daarna kun je het
     * alleen uitlezen. Als je het wilt aanpassen dan moet parameter vraagOmWachtwoord
     * true worden.
     * */
//    leesWachtwoord((char *)wachtwoord, vraagOmWachtwoord);

    SecParams->Key = netwerkww;
    SecParams->KeyLen = strlen((const char*)wachtwoord);
    SecParams->Type = SL_WLAN_SEC_TYPE_WPA_WPA2;

    // extended alleen nodig voor enterprise wifi
    SecExtParams->User = IDENTITY;
    SecExtParams->UserLen = strlen(IDENTITY);
    SecExtParams->AnonUser = 0;
    SecExtParams->AnonUserLen = 0;
    SecExtParams->EapMethod = SL_WLAN_ENT_EAP_METHOD_PEAP0_MSCHAPv2;

    // Jaar instellen zodat het certificaat niet wordt afgekeurd
    dateTime.tm_year =  (_u32)2019; // Year (YYYY format)

    // NWP herstarten om STA modus ook te gebruiken
    sl_Stop(0);
    sl_Start(0, 0, 0);

    // Tijd opslaan
    sl_DeviceSet(SL_DEVICE_GENERAL,
                 SL_DEVICE_GENERAL_DATE_TIME,
                 sizeof(SlDateTime_t),
                 (_u8 *)(&dateTime));

    // Eduroam geeft geen CA certificaat uit, dus check uitzetten
    Status = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, SL_WLAN_GENERAL_PARAM_DISABLE_ENT_SERVER_AUTH, 1, &serverAuthenticatie);
    if(Status == 0){
        UART_PRINT("Connection successful-ish\r\n");
        UART_PRINT("If it stops here: check password configs or\r\n"
                    "the network is still up");
    }

    return Status;
}
